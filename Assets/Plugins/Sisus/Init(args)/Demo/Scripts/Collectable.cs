﻿using Sisus.Init;
using UnityEngine;
using UnityEngine.Events;

namespace Init.Demo
{
    /// <summary>
    /// An object that can be collected by the player object.
    /// </summary>
    [AddComponentMenu("Initialization/Demo/Collectable")]
    public class Collectable : MonoBehaviour<UnityEvent>, ICollectable
    {
        [SerializeField, Tooltip("Event invoked when the object is collected.")]
        private UnityEvent collected = new UnityEvent();

        /// <inheritdoc/>
        public void Collect()
        {
            collected.Invoke();
            gameObject.SetActive(false);
        }

        /// <inheritdoc/>
        protected override void Init(UnityEvent collected)
        {
            this.collected = collected;
        }
    }
}
