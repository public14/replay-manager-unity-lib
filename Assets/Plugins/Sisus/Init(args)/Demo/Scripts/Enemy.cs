using UnityEngine;

namespace Init.Demo
{
    /// <summary>
    /// An enemy that kills the player object on collision.
    /// </summary>
    public class Enemy : MonoBehaviour, IDeadly { }
}
