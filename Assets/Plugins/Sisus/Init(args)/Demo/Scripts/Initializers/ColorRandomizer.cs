﻿using Sisus.Init;
using UnityEngine;

namespace Init.Demo
{
    /// <summary>
    /// Initializer for <see cref="Colored"/>.
    /// </summary>
    [AddComponentMenu("Initialization/Demo/Initializers/Color Randomizer")]
    public class ColorRandomizer : Initializer<Colored, RandomColor> { }
}