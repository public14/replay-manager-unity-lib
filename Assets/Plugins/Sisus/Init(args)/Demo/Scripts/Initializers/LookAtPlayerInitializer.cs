﻿using System;
using UnityEngine;
using Sisus.Init;

namespace Init.Demo
{
    [AddComponentMenu("Initialization/Demo/Initializers/Look At Player Initializer")]
    public sealed class LookAtPlayerInitializer : MonoBehaviour, IInitializer
    {
        [SerializeField]
        private LookAt client = null;
        
        [SerializeField]
        private Any<IPlayer> player = default;

        /// <inheritdoc/>
        object IInitializer.Target => client;

        /// <inheritdoc/>
        Type IInitializer.TargetType => typeof(LookAt);

        /// <inheritdoc/>
		object IInitializer.InitTarget() => InitTarget();

        private void Awake() => InitTarget();

		private LookAt InitTarget()
        {
            ((IInitializable<ITrackable>)client).Init(player.Value.Trackable);
            Destroy(this);
            return client;
        }
    }
}