﻿using Sisus.Init;

namespace Init.Demo
{
    /// <summary>
    /// <see cref="Initializer"/> for the <see cref="Movable"/> component.
    /// </summary>
    public sealed class MovableInitializer : Initializer<Movable, IInputManager, IMoveSettings, ILevel, ITimeProvider> { }
}