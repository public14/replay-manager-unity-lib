﻿using Sisus.Init;

namespace Init.Demo
{
    /// <summary>
    /// <see cref="Initializer"/> for the <see cref="Player"/> component.
    /// </summary>
    public sealed class PlayerInitializer : Initializer<Player, ITrackable> { }
}