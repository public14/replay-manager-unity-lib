﻿using Sisus.Init;

namespace Init.Demo
{
    /// <summary>
    /// <see cref="Initializer"/> for the <see cref="ResetHandler"/> component.
    /// </summary>
    public class ResetHandlerInitializer : Initializer<ResetHandler, IInputManager> { }
}