﻿using Sisus.Init;

namespace Init.Demo
{
    /// <summary>
    /// <see cref="Initializer"/> for the <see cref="SpawnerComponent"/>.
    /// </summary>
    public sealed class SpawnerInitializer : WrapperInitializer<SpawnerComponent, Spawner, SpawnerSettings, IPlayer, ILevel, ITimeProvider, IGameObjectBuilder>
    {
        /// <inheritdoc/>
        protected override Spawner CreateWrappedObject(SpawnerSettings spawnerSettings, IPlayer player, ILevel level, ITimeProvider timeProvider, IGameObjectBuilder gameObjectBuilder)
        {
            return new Spawner(spawnerSettings, player.Trackable, level, timeProvider, gameObjectBuilder);
        }
    }
}