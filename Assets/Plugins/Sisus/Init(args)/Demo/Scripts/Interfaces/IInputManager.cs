using System;
using UnityEngine;

namespace Init.Demo
{
    /// <summary>
    /// Represents the method that will handle an <see cref="IInputManager.MoveInputChanged"/> event.
    /// </summary>
    /// <param name="moveInput"> The move input given on the horizonal (x) and vertical (y) axes. </param>
    public delegate void MoveInputChangedHandler(Vector2 moveInput);

    /// <summary>
    /// Represents a manager responsible for detecting changes in user input
    /// and broadcasting events in reponse.
    /// </summary>
    public interface IInputManager
    {
        /// <summary>
        /// Event broadcast when move input given by the user has changed.
        /// </summary>
        event MoveInputChangedHandler MoveInputChanged;

        /// <summary>
        /// Event broadcast when reset input has been given by the user.
        /// </summary>
        event Action ResetInputGiven;
    }
}
