using UnityEngine;

namespace Init.Demo
{
    /// <summary>
    /// Represents the method that will handle an <see cref="ITrackable.PositionChanged"/> event.
    /// </summary>
    /// <param name="mover"> The object that has moved. </param>
    public delegate void PositionChangedHandler(ITrackable mover);

    /// <summary>
    /// Represents an object whose position can be tracked.
    /// </summary>
    public interface ITrackable
    {
        /// <summary>
        /// Event that is invoked whenever the position of the trackable object has changed.
        /// </summary>
        event PositionChangedHandler PositionChanged;

        /// <summary>
        /// The position of the object in world space.
        /// </summary>
        Vector2 Position { get; }
    }
}
