﻿using UnityEngine;
using Sisus.Init;

namespace Init.Demo
{
    /// <summary>
    /// Represents an object that continuously turns to face a target.
    /// </summary>
    [AddComponentMenu("Initialization/Demo/Look At")]
    public class LookAt : MonoBehaviour<ITrackable>
    {
        private ITrackable target;

        /// <inheritdoc/>
        protected override void Init(ITrackable target) => this.target = target;

        private void Update()
        {
            if(target != NullOrInactive)
            {
                transform.LookAt(target.Position);
            }
        }
    }
}