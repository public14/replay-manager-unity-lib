﻿using System;
using UnityEngine;
using Sisus.Init;

namespace Init.Demo
{
    /// <summary>
    /// A <see cref="GameObject"/> with the <see cref="Movable"/> component can move along
    /// the x and y axis within the <see cref="ILevel.Bounds">bounds</see> of the <see cref="ILevel"/>
    /// in reaction to <see cref="IInputManager.MoveInputChanged">movement input</see>.
    /// </para>
    /// </summary>
    [AddComponentMenu("Initialization/Demo/Movable")]
    public sealed class Movable : MonoBehaviour<IInputManager, IMoveSettings, ILevel, ITimeProvider>, IResettable, ITrackable
    {
        /// <inheritdoc/>
        public event PositionChangedHandler PositionChanged;

        private IInputManager inputManager;
        private IMoveSettings settings;
        private ILevel level;
        private ITimeProvider timeProvider;

        private Vector2 moveDirection;

        /// <inheritdoc/>
        public Vector2 Position => transform.position;

        /// <inheritdoc/>
        protected override void Init(IInputManager inputManager, IMoveSettings settings, ILevel level, ITimeProvider timeProvider)
        {
            if(inputManager is null) throw new ArgumentNullException(nameof(inputManager));
            if(settings is null) throw new ArgumentNullException(nameof(settings));
            if(level is null) throw new ArgumentNullException(nameof(level));
            if(timeProvider is null) throw new ArgumentNullException(nameof(timeProvider));

            this.inputManager = inputManager;
            this.settings = settings;
            this.level = level;
            this.timeProvider = timeProvider;
        }

        /// <inheritdoc/>
        void IResettable.ResetState()
        {
            transform.position = (Vector2)level.Bounds.min;
        }

        private void OnEnable()
        {
            inputManager.MoveInputChanged += OnMoveInputChanged;
        }

        private void OnMoveInputChanged(Vector2 moveInput)
        {
            moveDirection = moveInput;
        }

        private void Update()
        {
            if(moveDirection == Vector2.zero)
            {
                return;
            }

            float time = timeProvider.DeltaTime;
            float speed = settings.MoveSpeed;
            float distance = time * speed;

            Vector3 currentPosition = transform.position;
            Vector3 translation = moveDirection * distance;
            Vector3 updatedPosition = currentPosition + translation;

            if(updatedPosition.x < level.Bounds.x)
            {
                updatedPosition.x = level.Bounds.x;
            }
            if(updatedPosition.x > level.Bounds.xMax)
            {
                updatedPosition.x = level.Bounds.xMax;
            }
            if(updatedPosition.y < level.Bounds.y)
            {
                updatedPosition.y = level.Bounds.y;
            }
            if(updatedPosition.y > level.Bounds.yMax)
            {
                updatedPosition.y = level.Bounds.yMax;
            }

            if(updatedPosition == currentPosition)
            {
                return;
            }

            transform.position = updatedPosition;

            PositionChanged?.Invoke(this);
        }

        private void OnDisable()
        {
            OnMoveInputChanged(Vector2.zero);

            if(inputManager != Null)
            {
                inputManager.MoveInputChanged -= OnMoveInputChanged;
            }
        }
    }
}