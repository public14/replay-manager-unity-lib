﻿using Sisus.Init;
using UnityEngine;

namespace Init.Demo
{
    /// <summary>
    /// Class responsible for resetting all objects to their initial states on demand.
    /// </summary>
    [AddComponentMenu("Initialization/Demo/Reset Handler")]
    public class ResetHandler : MonoBehaviour<IInputManager>
    {
        private IInputManager inputManager;

        /// <inheritdoc/>
        protected override void Init(IInputManager inputManager)
        {
            this.inputManager = inputManager;
            inputManager.ResetInputGiven += OnResetInputGiven;
        }

        private void OnResetInputGiven()
        {
            foreach(var resettable in Find.All<IResettable>(true))
            {
                resettable.ResetState();
            }
        }

        private void OnDisable()
        {
            if(inputManager != null)
            {
                inputManager.ResetInputGiven -= OnResetInputGiven;
            }
        }
    }
}
