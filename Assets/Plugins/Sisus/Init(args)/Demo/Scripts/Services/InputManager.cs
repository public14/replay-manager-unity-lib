using Sisus.Init;
using System;
using UnityEngine;

namespace Init.Demo
{
    /// <summary>
    /// Manager responsible for detecting changes in user input and broadcasting events in reponse.
    /// </summary>
    [Service(typeof(IInputManager))]
    public sealed class InputManager : IInputManager, IUpdate, IInitializable<IInputSettings>
    {
        private KeyCode resetKey = KeyCode.R;
        private KeyCode moveLeftKey = KeyCode.LeftArrow;
        private KeyCode moveRightKey = KeyCode.RightArrow;
        private KeyCode moveDownKey = KeyCode.DownArrow;
        private KeyCode moveUpKey = KeyCode.UpArrow;

        private Vector2 previousMoveInput = Vector2.zero;

        /// <inheritdoc/>
        public event MoveInputChangedHandler MoveInputChanged;

        /// <inheritdoc/>
        public event Action ResetInputGiven;

        /// <inheritdoc/>
        public void Init(IInputSettings inputSettings)
        {
            resetKey = inputSettings.ResetKey;
            moveLeftKey = inputSettings.MoveLeftKey;
            moveRightKey = inputSettings.MoveRightKey;
            moveDownKey = inputSettings.MoveDownKey;
            moveUpKey = inputSettings.MoveUpKey;
        }

        /// <inheritdoc/>
        public void Update(float deltaTime)
        {
            if(Input.GetKeyDown(resetKey))
            {
                ResetInputGiven?.Invoke();
            }

            Vector2 moveInput = GetMoveInput();

            if(previousMoveInput == moveInput)
            {
                return;
            }

            previousMoveInput = moveInput;
            MoveInputChanged?.Invoke(moveInput);
        }

        private Vector2 GetMoveInput()
        {
            Vector2 input = Vector2.zero;

            if(Input.GetKey(moveRightKey))
            {
                input.x += 1f;
            }

            if(Input.GetKey(moveLeftKey))
            {
                input.x -= 1f;
            }

            if(Input.GetKey(moveUpKey))
            {
                input.y += 1f;
            }

            if(Input.GetKey(moveDownKey))
            {
                input.y -= 1f;
            }

            return input;
        }
    }
}
