﻿using Sisus.Init;
using UnityEngine;

namespace Init.Demo
{
    /// <summary>
    /// SciptableObject asset that holds settings data for user inputs
    /// that the player can be use to control the game.
    /// </summary>
    [Service(typeof(IInputSettings), ResourcePath = "InputSettings")]
    [Service(typeof(IResetInputSettings), ResourcePath = "InputSettings")]
    [Service(typeof(IMoveInputSettings), ResourcePath = "InputSettings")]
    [CreateAssetMenu]
    public sealed class InputSettings : ScriptableObject, IInputSettings
    {
        [SerializeField]
        private KeyCode resetKey = KeyCode.R;

        [SerializeField]
        private KeyCode moveLeftKey = KeyCode.LeftArrow;

        [SerializeField]
        private KeyCode moveRightKey = KeyCode.RightArrow;

        [SerializeField]
        private KeyCode moveUpKey = KeyCode.UpArrow;

        [SerializeField]
        private KeyCode moveDownKey = KeyCode.DownArrow;

        /// <inheritdoc/>
        public KeyCode ResetKey => resetKey;

        /// <inheritdoc/>
        public KeyCode MoveLeftKey => moveLeftKey;

        /// <inheritdoc/>
        public KeyCode MoveRightKey => moveRightKey;

        /// <inheritdoc/>
        public KeyCode MoveUpKey => moveUpKey;

        /// <inheritdoc/>
        public KeyCode MoveDownKey => moveDownKey;
    }
}