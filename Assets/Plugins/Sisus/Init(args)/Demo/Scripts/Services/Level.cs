﻿using UnityEngine;
using Sisus.Init;

namespace Init.Demo
{
    /// <summary>
    /// Represents a level in the game, defining the bounds inside which
    /// game objects can be positioned.
    /// </summary>
    [Service(typeof(ILevel), FindFromScene = true)]
    public sealed class Level : MonoBehaviour<RectInt>, ILevel
    {
        [SerializeField]
        private RectInt bounds = new RectInt(0, 0, 19, 19);

        /// <inheritdoc/>
        public RectInt Bounds => bounds;

        /// <inheritdoc/>
        /// <param name="bounds"> The position and size of the level. </param>
        protected override void Init(RectInt bounds) => this.bounds = bounds;
    }
}