﻿using UnityEngine;
using Sisus.Init;

namespace Init.Demo
{
    /// <summary>
    /// The player can collect <see cref="ICollectable">ICollectables</see>
    /// by entering the radius of their trigger and its position can be tracked
    /// via the <see cref="Trackable"/> property.
    /// </summary>
    [Service(typeof(IPlayer), FindFromScene = true)]
    public sealed class Player : ConstructorBehaviour<ITrackable>, IPlayer
    {
        /// <summary>
        /// Returns an object that allows keeping track of the <see cref="ITrackable.Position">position</see> of the <see cref="Player"/>.
        /// </summary>
        public ITrackable Trackable { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> component with
        /// the argument provided to the base constructor.
        /// </summary>
        public Player() : base(out ITrackable trackable) => Trackable = trackable;

        /// <inheritdoc/>
        protected override void ValidateArgument(ITrackable trackable) => AssertNotNull(trackable);

        private void OnTriggerEnter(Collider other)
        {
            if(other.TryGetComponent(out ICollectable collectable))
            {
                collectable.Collect();
            }
        }
    }
}