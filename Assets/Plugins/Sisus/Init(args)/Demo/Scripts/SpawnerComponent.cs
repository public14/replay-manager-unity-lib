﻿using Sisus.Init;
using UnityEngine;

namespace Init.Demo
{
    /// <summary>
    /// Wrapper component for <see cref="Spawner"/>.
    /// </summary>
    [AddComponentMenu("Initialization/Demo/Spawner")]
    public class SpawnerComponent : Wrapper<Spawner> { }
}