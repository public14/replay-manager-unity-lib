﻿using NUnit.Framework;
using UnityEngine.Events;
using Sisus.Init;
using Sisus.Init.Testing;

namespace Init.Demo.Tests
{
    /// <summary>
    /// Unit tests for <see cref="Collectable"/>.
    /// </summary>
    public class TestCollectable
    {
        private UnityEvent unityEvent;
        private Collectable collectable;
        private Testable testable;
        private bool unityEventInvoked = false;

        [SetUp]
        public void Setup()
        {
            unityEvent = new UnityEvent();
            unityEvent.AddListener(SetInvokedTrue);
            collectable = new GameObject<Collectable>().Init(unityEvent);
            testable = new Testable(collectable.gameObject);
        }

        private void SetInvokedTrue() => unityEventInvoked = true;

        [TearDown]
        public void TearDown()
        {
            testable.Destroy();
            unityEventInvoked = false;
        }

        [Test]
        public void Collect_Invokes_UnityEvent()
        {
            unityEventInvoked = false;
            collectable.Collect();
            Assert.IsTrue(unityEventInvoked);
        }

        [Test]
        public void Collect_Sets_GameObject_Inactive()
        {
            collectable.Collect();
            Assert.IsFalse(collectable.gameObject.activeSelf);
        }
    }
}