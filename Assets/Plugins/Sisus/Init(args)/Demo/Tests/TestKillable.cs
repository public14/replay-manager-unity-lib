﻿using UnityEngine;
using Object = UnityEngine.Object;
using NUnit.Framework;
using Sisus.Init;
using Sisus.Init.Testing;

namespace Init.Demo.Tests
{
    /// <summary>
    /// Unit tests for <see cref="Killable"/>.
    /// </summary>
    public class TestKillable
    {
        private Killable killable;
        private Testable testable;
        private ILogger loggerWas;
        private bool hasBeenKilled;

        [SetUp]
        public void Setup()
        {
            loggerWas = Service<ILogger>.Instance;
            Service.SetInstance<ILogger>(new NullLogger());

            killable = new GameObject<BoxCollider, Killable>();
            Assert.IsTrue(killable != null);
            Assert.IsNotNull(killable.Killed);
            killable.Killed.AddListener(OnPlayerKilled);
            hasBeenKilled = false;
            testable = new Testable(killable.gameObject);
        }

        private void OnPlayerKilled()
        {
            hasBeenKilled = true;
        }

        [TearDown]
        public void TearDown()
        {
            testable.Destroy();
            Service.SetInstance(loggerWas);
            hasBeenKilled = false;
        }

        [Test]
        public void Kill_Sets_GameObject_Inactive()
        {
            killable.Kill();
            Assert.IsFalse(killable.gameObject.activeSelf);
        }

        [Test]
        public void Reset_Sets_GameObject_Active()
        {
            killable.Kill();
            ((IResettable)killable).ResetState();
            Assert.IsTrue(killable.gameObject.activeSelf);
        }

        [Test]
        public void OnTriggerEnter_With_Deadly_Object_Kills()
        {
            (MockDeadly deadly, BoxCollider collider) deadly = new GameObject<MockDeadly, BoxCollider>();

            hasBeenKilled = false;
            testable.OnTriggerEnter(deadly.collider);
            Assert.IsTrue(hasBeenKilled);

            Object.DestroyImmediate(deadly.collider.gameObject);
        }
    }
}