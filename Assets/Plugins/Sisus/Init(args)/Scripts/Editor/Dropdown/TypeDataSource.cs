﻿using Sisus.Init.Internal;
using System;
using System.Collections.Generic;
using Object = UnityEngine.Object;

namespace Sisus.Init.EditorOnly
{
    internal class TypeDataSource : AdvancedDropdownDataSource
    {
        private readonly IEnumerable<Type> types;
        private readonly char namespaceDelimiter;
        private readonly string title;

        public TypeDataSource(IEnumerable<Type> types, bool useGroups, string title)
        {
            this.types = types;
            namespaceDelimiter = useGroups ? '/' : '\0';
            this.title = title;
        }

        protected override AdvancedDropdownItem GetData()
        {
            AdvancedDropdownItem root = new TypeDropdownItem(title);

            var enumerator = types.GetEnumerator();

            if(!enumerator.MoveNext())
            {
                return root;
            }
            int index = 0;

            var first = enumerator.Current;
            if(first is null)
            {
                AddLeafItem(root, "Null", "Null", index);
            }
            else
            {
                AddType(root, index, first);
            }

            if(!enumerator.MoveNext())
            {
                return root;
            }
            index++;

            bool addSeparator;
            var second = enumerator.Current;
            if(second == typeof(Object))
            {
                root.AddSeparator();

                AddLeafItem(root, "Object", "Object", index);
                addSeparator = true;
            }
            else
            {
                AddType(root, index, second);
                addSeparator = false;
            }

            if(!enumerator.MoveNext())
            {
                return root;
            }
            index++;

            if(addSeparator)
            {
                root.AddSeparator();
            }

            do
            {
                var type = enumerator.Current;
                AddType(root, index, type);
                index++;
            }
            while(enumerator.MoveNext());

            return root;
        }

        private void AddType(AdvancedDropdownItem root, int index, Type type)
        {
            var fullPath = TypeUtility.ToString(type, namespaceDelimiter);
            var pathParts = fullPath.Split('/');

            var parent = root;
            for(int i = 0, lastIndex = pathParts.Length - 1; i <= lastIndex; i++)
            {
                var name = pathParts[i];

                bool isGroup = i < lastIndex;

                if(isGroup)
                {
                    parent = GetOrAddGroup(parent, name);
                    continue;
                }

                AddLeafItem(parent, name, fullPath, index);
            }
        }

        private static AdvancedDropdownItem GetOrAddGroup(AdvancedDropdownItem parent, string name)
        {
            foreach(var existing in parent.children)
            {
                if(string.Equals(existing.name, name))
                {
                    return existing;
                }
            }

            var group = new TypeDropdownItem(name, -1);
            group.SetParent(parent);
            parent.AddChild(group);
            return group;
        }

        private static void AddLeafItem(AdvancedDropdownItem parent, string name, string fullPath, int index)
        {
            var element = new TypeDropdownItem(name, fullPath, index);
            element.searchable = true;
            element.SetParent(parent);
            parent.AddChild(element);
        }
    }
}
