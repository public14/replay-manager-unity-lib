﻿using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Sisus.Init.EditorOnly
{
    internal class TypeDropdownItem : AdvancedDropdownItem
    {
        private string m_MenuPath;

        private static class Styles
        {
            public static GUIStyle itemStyle = new GUIStyle("PR Label");
            public static Texture2D groupIcon;
            public static Texture2D typeIcon;

            static Styles()
            {
                itemStyle.alignment = TextAnchor.MiddleLeft;
                itemStyle.padding.left = 0;
                itemStyle.fixedHeight = 20;
                itemStyle.margin = new RectOffset(0, 0, 0, 0);

                groupIcon = EditorGUIUtility.IconContent("Folder Icon").image as Texture2D;
                typeIcon = null;
            }
        }

        public override GUIStyle lineStyle => Styles.itemStyle;

        internal TypeDropdownItem(string menuTitle) : base(menuTitle, -1)
        {
        }

        public TypeDropdownItem(string path, int index) : base(path, index)
        {
            m_Content = new GUIContent(path, Styles.groupIcon);            
        }

        public TypeDropdownItem(string name, string menuPath, int index) : base(name, index)
        {
            m_MenuPath = menuPath;
            m_Content = new GUIContent(name, Styles.typeIcon);
            m_ContentWhenSearching = new GUIContent(m_Content);
        }

        public override bool OnAction()
        {
            var types = TypeDropdownWindow.types;
            var type = types.ElementAtOrDefault(m_Index);
            TypeDropdownWindow.instance.OnTypeSelected(type);
            return true;
        }

        public override string ToString()
        {
            return m_MenuPath;
        }
    }
}
