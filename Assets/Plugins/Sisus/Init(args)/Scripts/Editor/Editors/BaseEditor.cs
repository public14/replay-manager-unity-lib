﻿using System;
using UnityEditor;

namespace Sisus.Init
{
    /// <summary>
    /// Base class for custom editors that visualize the non-serialized fields of a target in play mode.
    /// </summary>
    public abstract class BaseEditor : Editor
    {
        private RuntimeFieldsDrawer runtimeFieldsDrawer;

        protected abstract Type BaseType { get; }

        protected virtual void OnEnable()
        {
            runtimeFieldsDrawer = new RuntimeFieldsDrawer(target, BaseType);
        }

        public override void OnInspectorGUI()
        {
            if(runtimeFieldsDrawer is null)
            {
                runtimeFieldsDrawer = new RuntimeFieldsDrawer(target, BaseType);
            }

            base.OnInspectorGUI();

            runtimeFieldsDrawer.Draw();
        }
    }
}