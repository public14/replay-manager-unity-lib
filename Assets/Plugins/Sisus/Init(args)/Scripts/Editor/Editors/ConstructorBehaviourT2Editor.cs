﻿using System;
using UnityEditor;

namespace Sisus.Init
{
    [CustomEditor(typeof(ConstructorBehaviour<,>), true, isFallback = true)]
    public class ConstructorBehaviourT2Editor : BaseConstructorBehaviourEditor
    {
        protected override Type BaseType => typeof(ConstructorBehaviour<,>);
    }
}