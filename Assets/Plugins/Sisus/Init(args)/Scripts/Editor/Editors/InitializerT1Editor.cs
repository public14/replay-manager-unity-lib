﻿using UnityEngine;
using UnityEditor;

namespace Sisus.Init.EditorOnly
{
    using static InitializerEditorUtility;

    [CustomEditor(typeof(Initializer<,>), true)]
    public class InitializerT1Editor : Editor
    {
        private SerializedProperty client;
        private SerializedProperty argument;
        private SerializedProperty nullArgumentGuard;

        private GUIContent clientLabel;
        private GUIContent argumentLabel;

        private bool clientIsInitializable;
        private bool argumentIsService;

        private void Setup()
        {
            client = serializedObject.FindProperty("target");
            argument = serializedObject.FindProperty("argument");
            nullArgumentGuard = serializedObject.FindProperty(nameof(nullArgumentGuard));

            var genericArguments = target.GetType().BaseType.GetGenericArguments();
            
            var clientType = genericArguments[0];
            clientIsInitializable = IsInitializable(clientType);
            clientLabel = GetLabel(clientType);

            var argumentType = genericArguments[1];
            argumentIsService = IsService(argumentType);
            argumentLabel = GetLabel(argumentType);
        }

        public override void OnInspectorGUI()
        {
            if(client == null)
            {
                Setup();
            }

            DrawClientField(client, clientLabel, clientIsInitializable);

            GUILayout.Label("Init Argument", EditorStyles.boldLabel);

            bool nullAllowed = ((NullArgumentGuard)nullArgumentGuard.intValue).IsDisabled(NullArgumentGuard.EditModeWarning);
            DrawArgumentField(argument, argumentLabel, argumentIsService, nullAllowed);

            DrawOptions(nullArgumentGuard);

            serializedObject.ApplyModifiedProperties();
        }
    }
}