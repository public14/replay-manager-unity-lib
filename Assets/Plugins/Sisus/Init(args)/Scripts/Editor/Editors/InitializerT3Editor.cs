﻿using UnityEngine;
using UnityEditor;

namespace Sisus.Init.EditorOnly
{
    using static InitializerEditorUtility;

    [CustomEditor(typeof(Initializer<,,,>), true)]
    public class InitializerT3Editor : Editor
    {
        private SerializedProperty client;
        private SerializedProperty firstArgumentProperty;
        private SerializedProperty secondArgumentProperty;
        private SerializedProperty thirdArgumentProperty;
        private SerializedProperty nullArgumentGuard;

        private GUIContent clientLabel;
        private GUIContent firstArgumentLabel;
        private GUIContent secondArgumentLabel;
        private GUIContent thirdArgumentLabel;

        private bool clientIsInitializable;
        private bool firstArgumentIsService;
        private bool secondArgumentIsService;
        private bool thirdArgumentIsService;

        private void Setup()
        {
            client = serializedObject.FindProperty("target");
            firstArgumentProperty = serializedObject.FindProperty("firstArgument");
            secondArgumentProperty = serializedObject.FindProperty("secondArgument");
            thirdArgumentProperty = serializedObject.FindProperty("thirdArgument");
            nullArgumentGuard = serializedObject.FindProperty(nameof(nullArgumentGuard));

            var genericArguments = target.GetType().BaseType.GetGenericArguments();

            var clientType = genericArguments[0];
            clientIsInitializable = IsInitializable(clientType);
            clientLabel = GetLabel(clientType);

            var firstArgumentType = genericArguments[1];
            var secondArgumentType = genericArguments[2];
            var thirdArgumentType = genericArguments[3];
            firstArgumentIsService = IsService(firstArgumentType);
            secondArgumentIsService = IsService(secondArgumentType);
            thirdArgumentIsService = IsService(thirdArgumentType);
            firstArgumentLabel = GetLabel(firstArgumentType);
            secondArgumentLabel = GetLabel(secondArgumentType);
            thirdArgumentLabel = GetLabel(thirdArgumentType);
        }

        public override void OnInspectorGUI()
        {
            if(client == null)
            {
                Setup();
            }

            DrawClientField(client, clientLabel, clientIsInitializable);

            GUILayout.Label("Init Arguments", EditorStyles.boldLabel);

            bool nullAllowed = ((NullArgumentGuard)nullArgumentGuard.intValue).IsDisabled(NullArgumentGuard.EditModeWarning);
            DrawArgumentField(firstArgumentProperty, firstArgumentLabel, firstArgumentIsService, nullAllowed);
            DrawArgumentField(secondArgumentProperty, secondArgumentLabel, secondArgumentIsService, nullAllowed);
            DrawArgumentField(thirdArgumentProperty, thirdArgumentLabel, thirdArgumentIsService, nullAllowed);

            DrawOptions(nullArgumentGuard);

            serializedObject.ApplyModifiedProperties();
        }
    }
}