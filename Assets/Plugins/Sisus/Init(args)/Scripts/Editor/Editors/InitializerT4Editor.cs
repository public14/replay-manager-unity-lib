﻿using UnityEngine;
using UnityEditor;

namespace Sisus.Init.EditorOnly
{
    using static InitializerEditorUtility;

    [CustomEditor(typeof(Initializer<,,,,>), true)]
    public class InitializerT4Editor : Editor
    {
        private SerializedProperty client;
        private SerializedProperty firstArgumentProperty;
        private SerializedProperty secondArgumentProperty;
        private SerializedProperty thirdArgumentProperty;
        private SerializedProperty fourthArgumentProperty;
        private SerializedProperty nullArgumentGuard;

        private GUIContent clientLabel;
        private GUIContent firstArgumentLabel;
        private GUIContent secondArgumentLabel;
        private GUIContent thirdArgumentLabel;
        private GUIContent fourthArgumentLabel;

        private bool clientIsInitializable;
        private bool firstArgumentIsService;
        private bool secondArgumentIsService;
        private bool thirdArgumentIsService;
        private bool fourthArgumentIsService;

        private void Setup()
        {
            client = serializedObject.FindProperty("target");
            firstArgumentProperty = serializedObject.FindProperty("firstArgument");
            secondArgumentProperty = serializedObject.FindProperty("secondArgument");
            thirdArgumentProperty = serializedObject.FindProperty("thirdArgument");
            fourthArgumentProperty = serializedObject.FindProperty("fourthArgument");
            nullArgumentGuard = serializedObject.FindProperty(nameof(nullArgumentGuard));

            var genericArguments = target.GetType().BaseType.GetGenericArguments();

            var clientType = genericArguments[0];
            clientIsInitializable = IsInitializable(clientType);
            clientLabel = GetLabel(clientType);

            var firstArgumentType = genericArguments[1];
            var secondArgumentType = genericArguments[2];
            var thirdArgumentType = genericArguments[3];
            var fourthArgumentType = genericArguments[4];
            firstArgumentIsService = IsService(firstArgumentType);
            secondArgumentIsService = IsService(secondArgumentType);
            thirdArgumentIsService = IsService(thirdArgumentType);
            fourthArgumentIsService = IsService(fourthArgumentType);
            firstArgumentLabel = GetLabel(firstArgumentType);
            secondArgumentLabel = GetLabel(secondArgumentType);
            thirdArgumentLabel = GetLabel(thirdArgumentType);
            fourthArgumentLabel = GetLabel(fourthArgumentType);
        }

        public override void OnInspectorGUI()
        {
            if(client == null)
            {
                Setup();
            }

            DrawClientField(client, clientLabel, clientIsInitializable);

            GUILayout.Label("Init Arguments", EditorStyles.boldLabel);

            bool nullAllowed = ((NullArgumentGuard)nullArgumentGuard.intValue).IsDisabled(NullArgumentGuard.EditModeWarning);
            DrawArgumentField(firstArgumentProperty, firstArgumentLabel, firstArgumentIsService, nullAllowed);
            DrawArgumentField(secondArgumentProperty, secondArgumentLabel, secondArgumentIsService, nullAllowed);
            DrawArgumentField(thirdArgumentProperty, thirdArgumentLabel, thirdArgumentIsService, nullAllowed);
            DrawArgumentField(fourthArgumentProperty, fourthArgumentLabel, fourthArgumentIsService, nullAllowed);

            DrawOptions(nullArgumentGuard);

            serializedObject.ApplyModifiedProperties();
        }
    }
}