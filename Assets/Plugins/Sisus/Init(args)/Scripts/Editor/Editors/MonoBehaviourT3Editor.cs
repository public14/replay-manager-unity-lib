﻿using System;
using UnityEditor;

namespace Sisus.Init
{
    [CustomEditor(typeof(MonoBehaviour<,,>), true, isFallback = true)]
    public class MonoBehaviourT3Editor : BaseEditor
    {
        protected override Type BaseType
        {
            get
            {
                for(Type baseType = target.GetType().BaseType; baseType != null; baseType = baseType.BaseType)
                {
                    if(baseType.IsGenericType && baseType.GetGenericTypeDefinition() == typeof(MonoBehaviour<,,>))
                    {
                        return baseType;
                    }
                }

                return typeof(MonoBehaviour<,,>);
            }
        }
    }
}