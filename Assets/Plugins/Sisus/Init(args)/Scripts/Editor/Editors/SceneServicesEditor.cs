using UnityEditor;
using UnityEngine;

namespace Sisus.Init.EditorOnly
{
    [CustomEditor(typeof(Services), true, isFallback = true)]
    public class SceneServicesEditor : Editor
    {
        private SerializedProperty providesServices;
        private SerializedProperty toClients;

        private static readonly GUIContent clientsLabel = new GUIContent("For Clients");

        private void OnEnable() => Setup();
        
        private void Setup()
        {
            providesServices = serializedObject.FindProperty(nameof(providesServices));
            toClients = serializedObject.FindProperty(nameof(toClients));
        }

        public override void OnInspectorGUI()
        {
            if(providesServices is null)
            {
                Setup();
            }

            EditorGUILayout.PropertyField(providesServices);
            EditorGUILayout.PropertyField(toClients, clientsLabel);

            providesServices.serializedObject.ApplyModifiedProperties();
        }
    }
}