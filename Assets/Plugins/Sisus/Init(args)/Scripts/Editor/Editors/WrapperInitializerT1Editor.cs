﻿using UnityEngine;
using UnityEditor;

namespace Sisus.Init.EditorOnly
{
    using static InitializerEditorUtility;

    [CustomEditor(typeof(WrapperInitializer<,,,>), true)]
    public class WrapperInitializerT1Editor : Editor
    {
        private SerializedProperty client;
        private SerializedProperty argumentProperty;
        private SerializedProperty allowNullValue;

        private GUIContent clientLabel;
        private GUIContent argumentLabel;

        private bool argumentIsService;

        private void Setup()
        {
            client = serializedObject.FindProperty("target");
            argumentProperty = serializedObject.FindProperty("argument");
            allowNullValue = serializedObject.FindProperty("allowNullArgument");

            var genericArguments = target.GetType().BaseType.GetGenericArguments();

            var clientType = genericArguments[0];
            clientLabel = GetLabel(clientType);

            var argumentType = genericArguments[2];
            argumentIsService = IsService(argumentType);
            argumentLabel = GetLabel(argumentType);
        }

        public override void OnInspectorGUI()
        {
            if(client == null)
            {
                Setup();
            }

            DrawClientField(client, clientLabel, true);

            GUILayout.Label("Constructor Argument", EditorStyles.boldLabel);

            bool nullAllowed = allowNullValue.boolValue;
            DrawArgumentField(argumentProperty, argumentLabel, argumentIsService, nullAllowed);

            DrawOptions(allowNullValue);

            serializedObject.ApplyModifiedProperties();
        }
    }
}