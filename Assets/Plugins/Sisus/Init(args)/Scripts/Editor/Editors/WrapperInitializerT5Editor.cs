﻿using UnityEngine;
using UnityEditor;

namespace Sisus.Init.EditorOnly
{
    using static InitializerEditorUtility;

    [CustomEditor(typeof(WrapperInitializer<,,,,,,>), true)]
    public class WrapperInitializerT5Editor : Editor
    {
        private SerializedProperty client;
        private SerializedProperty firstArgumentProperty;
        private SerializedProperty secondArgumentProperty;
        private SerializedProperty thirdArgumentProperty;
        private SerializedProperty fourthArgumentProperty;
        private SerializedProperty fifthArgumentProperty;
        private SerializedProperty nullArgumentGuard;

        private GUIContent clientLabel;
        private GUIContent firstArgumentLabel;
        private GUIContent secondArgumentLabel;
        private GUIContent thirdArgumentLabel;
        private GUIContent fourthArgumentLabel;
        private GUIContent fifthArgumentLabel;

        private bool firstArgumentIsService;
        private bool secondArgumentIsService;
        private bool thirdArgumentIsService;
        private bool fourthArgumentIsService;
        private bool fifthArgumentIsService;

        private void Setup()
        {
            client = serializedObject.FindProperty("target");
            firstArgumentProperty = serializedObject.FindProperty("firstArgument");
            secondArgumentProperty = serializedObject.FindProperty("secondArgument");
            thirdArgumentProperty = serializedObject.FindProperty("thirdArgument");
            fourthArgumentProperty = serializedObject.FindProperty("fourthArgument");
            fifthArgumentProperty = serializedObject.FindProperty("fifthArgument");
            nullArgumentGuard = serializedObject.FindProperty(nameof(nullArgumentGuard));

            var genericArguments = target.GetType().BaseType.GetGenericArguments();

            var clientType = genericArguments[0];
            clientLabel = GetLabel(clientType);

            var firstArgumentType = genericArguments[2];
            var secondArgumentType = genericArguments[3];
            var thirdArgumentType = genericArguments[4];
            var fourthArgumentType = genericArguments[5];
            var fifthArgumentType = genericArguments[6];
            firstArgumentIsService = IsService(firstArgumentType);
            secondArgumentIsService = IsService(secondArgumentType);
            thirdArgumentIsService = IsService(thirdArgumentType);
            fourthArgumentIsService = IsService(fourthArgumentType);
            fifthArgumentIsService = IsService(fifthArgumentType);
            firstArgumentLabel = GetLabel(firstArgumentType);
            secondArgumentLabel = GetLabel(secondArgumentType);
            thirdArgumentLabel = GetLabel(thirdArgumentType);
            fourthArgumentLabel = GetLabel(fourthArgumentType);
            fifthArgumentLabel = GetLabel(fifthArgumentType);
        }

        public override void OnInspectorGUI()
        {
            if(client == null)
            {
                Setup();
            }

            DrawClientField(client, clientLabel, true);

            GUILayout.Label("Constructor Arguments", EditorStyles.boldLabel);

            bool nullAllowed = ((NullArgumentGuard)nullArgumentGuard.intValue).IsDisabled(NullArgumentGuard.EditModeWarning);
            DrawArgumentField(firstArgumentProperty, firstArgumentLabel, firstArgumentIsService, nullAllowed);
            DrawArgumentField(secondArgumentProperty, secondArgumentLabel, secondArgumentIsService, nullAllowed);
            DrawArgumentField(thirdArgumentProperty, thirdArgumentLabel, thirdArgumentIsService, nullAllowed);
            DrawArgumentField(fourthArgumentProperty, fourthArgumentLabel, fourthArgumentIsService, nullAllowed);
            DrawArgumentField(fifthArgumentProperty, fifthArgumentLabel, fifthArgumentIsService, nullAllowed);

            DrawOptions(nullArgumentGuard);

            serializedObject.ApplyModifiedProperties();
        }
    }
}