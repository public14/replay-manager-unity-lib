﻿using System;
using System.Reflection;
using System.Runtime.Serialization;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Sisus.Init.EditorOnly
{
	internal static class InitializerEditorUtility
	{
		private static readonly GUIContent clientNullHelpBoxText = new GUIContent("Client will be added to this GameObject during initialization.");
		private static readonly GUIContent clientPrefabHelpBoxText = new GUIContent("Client is a prefab instance will be added to GameObject during initialization.");
		private static readonly GUIContent clientNotInitializableHelpBoxText = new GUIContent("Can not inject arguments to client because it does not implement IInitializable.");
		private static readonly GUIContent serviceLabel = new GUIContent("Service", "A shared instance of this service will be automatically provided for the client during initialization.");
		private static bool optionsUnfolded;
		private static readonly GUIContent valueText = new GUIContent("Value");
		private static readonly GUIContent blankLabel = new GUIContent(" ");

		internal static void DrawArgumentField(SerializedProperty argumentProperty, GUIContent label, bool isService, bool canBeNull)
		{
			if(isService)
			{
				var labelRect = EditorGUILayout.GetControlRect();
				var controlRect = EditorGUI.PrefixLabel(labelRect, blankLabel);
				
				GUI.enabled = false;
				GUI.Label(controlRect, serviceLabel);
				GUI.enabled = true;

				labelRect.width -= controlRect.width;
				GUI.Label(labelRect, label);
				return;
			}

			if(argumentProperty == null)
			{
				GUI.color = Color.red;
				EditorGUILayout.LabelField(label.text, "NULL");
				GUI.color = Color.white;
				return;
			}

			bool invalidNullValue = false;
			if(!canBeNull)
            {
				var any = argumentProperty.GetValue();
				Type anyType = any.GetType();
				var targetObject = argumentProperty.serializedObject.targetObject;
				invalidNullValue = !(bool)anyType.GetMethod(nameof(Any<object>.GetHasValue)).MakeGenericMethod(new Type[] { targetObject.GetType() }).Invoke(any, new object[] { targetObject, Context.MainThread });
			}

			if(invalidNullValue)
			{
				GUILayout.BeginHorizontal();
				EditorGUILayout.PrefixLabel(label);

				GUI.color = Color.red;
				EditorGUILayout.PropertyField(argumentProperty, GUIContent.none);
				GUI.color = Color.white;

				GUILayout.EndHorizontal();
			}
			else
            {
				EditorGUILayout.PropertyField(argumentProperty, label);
			}

			argumentProperty.serializedObject.ApplyModifiedProperties();
		}

		internal static void DrawArgumentField(SerializedProperty argument, GUIContent label, Type type, bool isService, bool isUnityObject, bool canBeNull, FieldInfo fieldInfo)
		{
			if(isService)
			{
				var labelRect = EditorGUILayout.GetControlRect();
				
				var controlRect = EditorGUI.PrefixLabel(labelRect, blankLabel);
				
				GUI.enabled = false;
				GUI.Label(controlRect, "Service");
				GUI.enabled = true;

				labelRect.width -= controlRect.width;
				GUI.Label(labelRect, label);
				return;
			}

			if(argument == null)
			{
				GUI.color = Color.red;
				EditorGUILayout.LabelField(label.text, "NULL");
				GUI.color = Color.white;
				return;
			}

			if(isUnityObject)
			{
				GUI.color = argument.objectReferenceValue == null && !canBeNull ? Color.red : Color.white;
				EditorGUILayout.ObjectField(argument, type, label);
				GUI.color = Color.white;
			}
			else if(argument.propertyType != SerializedPropertyType.ManagedReference)
			{
				EditorGUILayout.PropertyField(argument, label, true);
			}
			else
			{
				var rect = EditorGUILayout.GetControlRect();
				var buttonRect = EditorGUI.PrefixLabel(rect, label);

				DrawArgumentFieldTypeSelectorButton(buttonRect, argument, type);

				var typeOfValue = GetType(argument, fieldInfo);
				if(typeOfValue is null)
                {
					return;
                }
				else if(typeOfValue.IsSerializable)
				{
					EditorGUI.indentLevel++;
					EditorGUILayout.PropertyField(argument, valueText, true);
					EditorGUI.indentLevel--;
				}
				else
				{
					EditorGUILayout.HelpBox(new GUIContent(typeOfValue.Name + " is missing the [Serializable] attribute."));
				}
			}

			argument.serializedObject.ApplyModifiedProperties();
		}

		private static Type GetType(SerializedProperty argument, FieldInfo fieldInfo)
		{
			var initializer = argument.serializedObject.targetObject;
			object value = fieldInfo.GetValue(initializer);
			return value is null ? null : value.GetType();
		}

		internal static Type GetInitializerType(SerializedObject serializedObject)
        {
			var initializerType = serializedObject.targetObject.GetType().BaseType;
            while(!initializerType.IsGenericType || (!initializerType.Name.StartsWith("Initializer") && !initializerType.Name.StartsWith("WrapperInitializer")))
            {
                initializerType = initializerType.BaseType;
            }
			return initializerType;
		}

		private static void DrawArgumentFieldTypeSelectorButton(Rect buttonRect, SerializedProperty argument, Type type)
		{
            string selectedTypeName = argument.type;
            if(selectedTypeName.StartsWith("managedReference<"))
            {
                selectedTypeName = selectedTypeName.Substring(17, selectedTypeName.Length - 18);
            }

			string buttonLabel = selectedTypeName.Length == 0 ? "Null" : ObjectNames.NicifyVariableName(selectedTypeName);
			if(!EditorGUI.DropdownButton(buttonRect, new GUIContent(buttonLabel), FocusType.Keyboard))
			{
				return;
            }

			var menu = new GenericMenu();

			menu.AddItem(new GUIContent("Null"), selectedTypeName.Length == 0, () => SetArgumentType(argument, null));

			if(!type.IsAbstract)
            {
				string typeName = type.Name;
				GUIContent itemLabel = new GUIContent(ObjectNames.NicifyVariableName(typeName), type.FullName);
				bool isSelected = string.Equals(typeName, selectedTypeName);
				menu.AddItem(itemLabel, isSelected, () => SetArgumentType(argument, type));
				menu.DropDown(buttonRect);
				return;
			}

			bool hasItems = false;
			foreach(Type derivedType in TypeCache.GetTypesDerivedFrom(type))
            {
				if(derivedType.IsAbstract || typeof(Object).IsAssignableFrom(derivedType) || derivedType.IsGenericTypeDefinition)
                {
					continue;
                }

				string derivedTypeName = derivedType.Name;
				GUIContent itemLabel = new GUIContent(ObjectNames.NicifyVariableName(derivedTypeName), derivedType.FullName);
				bool isSelected = string.Equals(derivedTypeName, selectedTypeName);
				menu.AddItem(itemLabel, isSelected, ()=> SetArgumentType(argument, derivedType));

				hasItems = true;
			}

			if(hasItems)
			{
				menu.DropDown(buttonRect);
			}
        }

		private static void SetArgumentType(SerializedProperty argument, Type type)
        {
			argument.managedReferenceValue = type == null ? null : CreateInstance(type);
			argument.serializedObject.ApplyModifiedProperties();
		}

		internal static object CreateInstance(Type type)
        {
			try
			{
				return Activator.CreateInstance(type);
			}
			catch(Exception)
			{
				return FormatterServices.GetUninitializedObject(type);
			}
        }

		internal static bool CanAssignUnityObjectToField(Type type)
		{
			if(typeof(Object).IsAssignableFrom(type))
			{
				return true;
			}

			if(!type.IsInterface)
			{
				return false;
			}

			foreach(var derivedType in TypeCache.GetTypesDerivedFrom(type))
			{
				if(typeof(Object).IsAssignableFrom(derivedType) && !derivedType.IsAbstract)
				{
					return true;
				}
			}

			return false;
		}

		internal static void DrawClientField(SerializedProperty client, GUIContent clientLabel, bool isInitializable)
		{
			GUILayout.Label("Client", EditorStyles.boldLabel);

			Object target = client.objectReferenceValue;

			if(target == null)
			{
				EditorGUILayout.HelpBox(clientNullHelpBoxText);
			}
			else if(target is Component component && !component.gameObject.scene.IsValid())
			{
				EditorGUILayout.HelpBox(clientPrefabHelpBoxText);
			}
			else if(!isInitializable)
            {
				EditorGUILayout.HelpBox(clientNotInitializableHelpBoxText);
				GUI.color = Color.red;
            }

			EditorGUILayout.PropertyField(client, clientLabel);

			GUI.color = Color.white;

			GUILayout.Space(5f);
		}

		internal static GUIContent GetLabel(Type type)
		{
			var name = ObjectNames.NicifyVariableName(type.Name);
			return new GUIContent(name.StartsWith("I ") ? name.Substring(2) : name);
		}

		internal static bool IsInitializable(Type clientType)
        {
			foreach(var interfaceType in clientType.GetInterfaces())
            {
				if(!interfaceType.IsGenericType)
                {
					continue;
                }
				var genericTypeDefinition = interfaceType.GetGenericTypeDefinition();
				if(genericTypeDefinition == typeof(IInitializable<>)
					|| genericTypeDefinition == typeof(IInitializable<,>)
					|| genericTypeDefinition == typeof(IInitializable<,,>)
					|| genericTypeDefinition == typeof(IInitializable<,,,>)
					|| genericTypeDefinition == typeof(IInitializable<,,,,>))
				{
					return true;
                }
			}
			return false;
        }

        internal static bool IsService(Type type) => ServiceUtility.IsServiceType(type);

        internal static void DrawOptions(SerializedProperty nullArgumentGuard)
        {
			GUI.color = Color.white;

            EditorGUILayout.Space(5f);

            optionsUnfolded = EditorGUILayout.Foldout(optionsUnfolded, "Options", EditorStyles.foldoutHeader);
            if(optionsUnfolded)
            {
				EditorGUI.indentLevel++;
                EditorGUILayout.PropertyField(nullArgumentGuard);
				EditorGUI.indentLevel--;
			}
        }
	}
}