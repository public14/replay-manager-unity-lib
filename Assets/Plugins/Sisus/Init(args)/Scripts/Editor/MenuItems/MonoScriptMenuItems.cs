using Sisus.Init.Internal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Sisus.Init
{
    internal static class MonoScriptMenuItems
    {
        private const string InitializerTemplate =
            "using Sisus.Init;\r\n" +
            "{0}\r\n" +
            "namespace {1}\r\n" +
            "{{\r\n" +
            "\t/// <summary>\r\n" +
            "\t/// <see cref=\"Initializer\"/> for the <see cref=\"{2}\"/> component.\r\n" +
            "\t/// </summary>\r\n" +
            "\tpublic sealed class {2}Initializer : Initializer<{2}, {3}> {{ }}\r\n" +
            "}}\r\n";

        private const string WrapperTemplate =
            "using UnityEngine;\r\n" +
            "using Sisus.Init;\r\n" +
            "\r\n" +
            "namespace {0}\r\n" +
            "{{\r\n" +
            "\t/// <summary>\r\n" +
            "\t/// <see cref=\"Wrapper{{}}\"/> for the <see cref=\"{1}\"/> component.\r\n" +
            "\t/// </summary>\r\n" +
            "\t[AddComponentMenu(\"Wrapper/{1}\")]\r\n" +
            "\tpublic sealed class {1}Component : Wrapper<{1}> {{ }}\r\n" +
            "}}\r\n";

        [MenuItem("CONTEXT/MonoScript/Create Initializer")]
        private static void CreateInitializerFromScript(MenuCommand command)
        {
            var script = command.context as MonoScript;
            CreateInitializer(AssetDatabase.GetAssetPath(script), script);
        }

        [MenuItem("CONTEXT/AssetImporter/Create Initializer")]
        private static void CreateInitializerFromScriptImporter(MenuCommand command)
        {
            var monoScriptImporter = command.context as AssetImporter;
            string assetPath = monoScriptImporter.assetPath;
            var script = AssetDatabase.LoadAssetAtPath<MonoScript>(assetPath);
            CreateInitializer(assetPath, script);
        }

        [MenuItem("CONTEXT/AssetImporter/Create Initializer", validate = true)]
        private static bool ShowCreateInitializerFromScriptImporter(MenuCommand command)
        {
            var assetImporter = command.context as AssetImporter;
            string assetPath = assetImporter.assetPath;
            var script = AssetDatabase.LoadAssetAtPath<MonoScript>(assetPath);
            if(script == null)
            {
                return false;
            }

            var type = script.GetClass();
            return type == null || typeof(MonoBehaviour).IsAssignableFrom(type);
        }


        [MenuItem("CONTEXT/MonoScript/Create Wrapper")]
        private static void CreateWrapperFromScript(MenuCommand command)
        {
            var script = command.context as MonoScript;
            CreateWrapper(AssetDatabase.GetAssetPath(script), script);
        }

        [MenuItem("CONTEXT/AssetImporter/Create Wrapper")]
        private static void CreateWrapperFromScriptImporter(MenuCommand command)
        {
            var monoScriptImporter = command.context as AssetImporter;
            string assetPath = monoScriptImporter.assetPath;
            var script = AssetDatabase.LoadAssetAtPath<MonoScript>(assetPath);
            CreateWrapper(assetPath, script);
        }

        [MenuItem("CONTEXT/AssetImporter/Create Wrapper", validate = true)]
        private static bool ShowCreateWrapperFromScriptImporter(MenuCommand command)
        {
            var assetImporter = command.context as AssetImporter;
            string assetPath = assetImporter.assetPath;
            
            var script = AssetDatabase.LoadAssetAtPath<MonoScript>(assetPath);
            if(script == null)
            {
                return false;
            }

            var type = script.GetClass();
            return type == null || (!typeof(Object).IsAssignableFrom(type) && !type.IsEnum && type.IsClass);
        }


        private static void CreateInitializer(string assetPath, MonoScript script)
        {
            var @class = script == null ? null : script.GetClass();
            string className = @class != null ? TypeUtility.ToString(@class) : Path.GetFileNameWithoutExtension(assetPath);
            var @namespace = @class?.Namespace ?? "MyNamespace";
            Type[] interfaces = @class?.GetInterfaces() ?? Type.EmptyTypes;
            CreateInitializer(assetPath, className, @namespace, interfaces);
        }

        private static void CreateInitializer(string forScriptAtPath, string className, string @namespace, Type[] interfaces)
        {
            var parameters = GetInitParameters(interfaces);
            string parameterNames;
            int parameterCount = parameters.Length;
            HashSet<string> namespaces = new HashSet<string>();
            if(parameterCount > 0)
            {
                string[] names = new string[parameterCount];
                for(int i = 0; i < parameterCount; i++)
                {
                    names[i] = NameOf(parameters[i], namespaces);
                }
                parameterNames = string.Join(", ", names);
            }
            else
            {
                parameterNames = "FirstArgument, ..., LastArgument";
            }

            if(@namespace != null)
            {
                namespaces.Remove(@namespace);
            }

            var usings = namespaces.Count == 0 ? "" : string.Join("", namespaces.Select(n => "using " + n + ";\r\n"));
            string code = string.Format(InitializerTemplate, usings, @namespace, className, parameterNames);
            string path = forScriptAtPath;
            path = Path.GetDirectoryName(path);
            path = Path.Combine(path, className + "Initializer.cs");
            File.WriteAllText(path, code);
            AssetDatabase.ImportAsset(path);
        }

        private static Type[] GetInitParameters(Type[] interfaces)
        {
            foreach(var @interface in interfaces)
            {
                var args = GetInitParameters(@interface);
                if(args.Length > 0)
                {
                    return args;
                }
            }

            return Type.EmptyTypes;
        }

        private static Type[] GetInitParameters(Type @interface)
        {
            if(!@interface.IsGenericType || @interface.IsGenericTypeDefinition)
            {
                return Type.EmptyTypes;
            }

            var definition = @interface.GetGenericTypeDefinition();
            if(definition == typeof(IArgs<,,,,>)
            || definition == typeof(IArgs<,,,>)
            || definition == typeof(IArgs<,,>)
            || definition == typeof(IArgs<,>)
            || definition == typeof(IArgs<>))
            {
                return @interface.GetGenericArguments();
            }

            return Type.EmptyTypes;
        }


        private static void CreateWrapper(string assetPath, MonoScript script)
        {
            var @class = script == null ? null : script.GetClass();
            string className = @class?.Name ?? Path.GetFileNameWithoutExtension(assetPath);
            var @namespace = @class?.Namespace ?? "MyNamespace";
            Type[] interfaces = @class?.GetInterfaces() ?? Type.EmptyTypes;
            CreateWrapper(assetPath, className, @namespace);
        }

        private static void CreateWrapper(string forScriptAtPath, string className, string @namespace)
        {
            string code = string.Format(WrapperTemplate, @namespace, className);
            string path = forScriptAtPath;
            path = Path.GetDirectoryName(path);
            path = Path.Combine(path, className + "Component.cs");
            File.WriteAllText(path, code);
            AssetDatabase.ImportAsset(path);
        }

        private static string NameOf(Type type, HashSet<string> namespaces)
        {
            if(type == typeof(int))
            {
                return "int";
            }
            if(type == typeof(string))
            {
                return "string";
            }
            if(type == typeof(float))
            {
                return "float";
            }
            if(type == typeof(double))
            {
                return "double";
            }
            if(type == typeof(object))
            {
                return "object";
            }

            if(type.Namespace != null)
            {
                namespaces.Add(type.Namespace);
            }

            return TypeUtility.ToString(type);
        }
    }
}