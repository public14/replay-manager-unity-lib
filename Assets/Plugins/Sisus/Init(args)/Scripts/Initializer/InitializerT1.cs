﻿#pragma warning disable CS0414

using JetBrains.Annotations;
using System;
using UnityEngine;

namespace Sisus.Init
{
    using static Internal.InitializerUtility;
	#if UNITY_EDITOR
	using static EditorOnly.AutoInitUtility;
	#endif

	/// <summary>
	/// A base class for a component that can be used to specify the argument used to
	/// initialize an object that implements <see cref="IInitializable{TArgument}"/>.
	/// <para>
	/// The argument value can be assigned using the inspector and serialized as part
	/// of a scene or a prefab.
	/// </para>
	/// <para>
	/// The argument gets injected to the <typeparamref name="TClient">client</typeparamref>
	/// during the <see cref="Awake"/> event.
	/// </para>
	/// <para>
	/// The client receives the argument via the <see cref="IInitializable{TArgument}.Init">Init</see>
	/// method where it can assign them to a member field or property.
	/// </para>
	/// <para>
	/// After the argument has been injected the <see cref="Initializer{,}"/> is removed from the
	/// <see cref="GameObject"/> that holds it.
	/// </para>
	/// </summary>
	/// <typeparam name="TClient"> Type of the initialized client component. </typeparam>
	/// <typeparam name="TArgument"> Type of the argument received by the client component. </typeparam>
	public abstract class Initializer<TClient, TArgument> : MonoBehaviour, IInitializer, IValueProvider<TClient>
		where TClient : MonoBehaviour, IInitializable<TArgument>
	{
		[SerializeField, Tooltip(TargetTooltip)]
		protected TClient target = null;

		[SerializeField]
		private Any<TArgument> argument;

		[SerializeField, Tooltip(NullArgumentGuardTooltip)]
		private NullArgumentGuard nullArgumentGuard = NullArgumentGuard.EditModeWarning | NullArgumentGuard.RuntimeException;

		/// <inheritdoc/>
		TClient IValueProvider<TClient>.Value => target;

		/// <inheritdoc/>
		object IValueProvider.Value => target;

		/// <inheritdoc/>
		object IInitializer.Target => target;

		/// <inheritdoc/>
		Type IInitializer.TargetType => typeof(TClient);

		/// <inheritdoc/>
		object IInitializer.InitTarget() => InitTarget();

		#if UNITY_EDITOR
		private void Reset()
		{
			OnReset(this, ref target);
			PrepareArgumentForAutoInit<TClient, TArgument>(this);
			ResetArgument(ref argument, 0, true, this);
		}
		#endif

        private void Awake() => InitTarget();

		private TClient InitTarget()
		{
			var argument = this.argument.GetValueOrDefault(this, Context.Awake);

			#if DEBUG || INIT_ARGS_SAFE_MODE
			if(argument is null && nullArgumentGuard.IsEnabled(NullArgumentGuard.RuntimeException))
			{
				throw GetMissingInitArgumentsException(GetType(), typeof(TClient), typeof(TArgument));
			}
			#endif

			target = InitTarget(argument);
			Destroy(this);
			return target;
		}

		/// <summary>
		/// Initializes the existing <see cref="Target"/> or new instance of type <see cref="TClient"/> using the provided argument.
		/// </summary>
		/// <param name="firstArgument"> The argument to pass to the target's Init function. </param>
		/// <returns> The existing <see cref="target"/> or new instance of type <see cref="TClient"/>. </returns>
		[NotNull]
		protected virtual TClient InitTarget(TArgument argument)
        {
            if(target == null)
            {
                return target = gameObject.AddComponent<TClient, TArgument>(argument);
            }

			if(!target.gameObject.scene.IsValid())
            {
                return target.Instantiate(argument);
            }
			
			target.Init(argument);
			return target;
        }

		#if UNITY_EDITOR
		private void OnValidate()
		{
			bool canBeNull = CanArgumentsBeNullInEditMode(gameObject, nullArgumentGuard);
			Validate(ref argument, this, canBeNull);
		}
		#endif
	}
}