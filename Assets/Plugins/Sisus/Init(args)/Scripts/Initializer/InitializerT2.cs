﻿#pragma warning disable CS0414

using JetBrains.Annotations;
using System;
using UnityEngine;

namespace Sisus.Init
{
	using static Internal.InitializerUtility;
	#if UNITY_EDITOR
	using static EditorOnly.AutoInitUtility;
	#endif

	/// <summary>
	/// A base class for a component that can can specify the two arguments used to
	/// initialize an object that implements <see cref="IInitializable{TFirstArgument, TSecondArgument}"/>.
	/// <para>
	/// The argument values can be assigned using the inspector and serialized as part of a scene or a prefab.
	/// </para>
	/// <para>
	/// The arguments get injected to the <typeparamref name="TClient">client</typeparamref>
	/// during the <see cref="Awake"/> event.
	/// </para>
	/// <para>
	/// The client receives the arguments via the
	/// <see cref="IInitializable{TFirstArgument, TSecondArgument}.Init">Init</see>
	/// method where it can assigned them to member fields or properties.
	/// </para>
	/// <para>
	/// After the arguments have been injected the <see cref="Initializer{,,}"/> is removed from the
	/// <see cref="GameObject"/> that holds it.
	/// </para>
	/// </summary>
	/// <typeparam name="TClient"> Type of the initialized client component. </typeparam>
	/// <typeparam name="TFirstArgument"> Type of the first argument received by the client component. </typeparam>
	/// <typeparam name="TSecondArgument"> Type of the second argument received by the client component. </typeparam>
	public abstract class Initializer<TClient, TFirstArgument, TSecondArgument> : MonoBehaviour, IInitializer, IValueProvider<TClient>
		where TClient : MonoBehaviour, IInitializable<TFirstArgument, TSecondArgument>
	{
		[SerializeField, Tooltip(TargetTooltip)]
		protected TClient target = null;

		[SerializeField]
		private Any<TFirstArgument> firstArgument = default;
		[SerializeField]
		private Any<TSecondArgument> secondArgument = default;

		[SerializeField, Tooltip(NullArgumentGuardTooltip)]
		private NullArgumentGuard nullArgumentGuard = NullArgumentGuard.EditModeWarning | NullArgumentGuard.RuntimeException;

		/// <inheritdoc/>
		TClient IValueProvider<TClient>.Value => target;

		/// <inheritdoc/>
		object IValueProvider.Value => target;

		/// <inheritdoc/>
		object IInitializer.Target => target;

		/// <inheritdoc/>
		Type IInitializer.TargetType => typeof(TClient);

		/// <inheritdoc/>
		object IInitializer.InitTarget() => InitTarget();

		#if UNITY_EDITOR
        private void Reset()
		{
			OnReset(this, ref target);
			PrepareArgumentsForAutoInit<TClient, TFirstArgument, TSecondArgument>(this);
			ResetArgument(ref firstArgument, 0, true, this);
			ResetArgument(ref secondArgument, 1, true, this);
		}
		#endif

		private void Awake() => InitTarget();

		private TClient InitTarget()
		{
			var firstArgument = this.firstArgument.GetValueOrDefault(this, Context.Awake);
			var secondArgument = this.secondArgument.GetValueOrDefault(this, Context.Awake);

			#if DEBUG || INIT_ARGS_SAFE_MODE
			if(nullArgumentGuard.IsEnabled(NullArgumentGuard.RuntimeException))
			{
				if(firstArgument is null) throw GetMissingInitArgumentsException(GetType(), typeof(TClient), typeof(TFirstArgument));
				if(secondArgument is null) throw GetMissingInitArgumentsException(GetType(), typeof(TClient), typeof(TSecondArgument));
			}
			#endif

			target = InitTarget(firstArgument, secondArgument);

			Destroy(this);

			return target;
		}

		/// <summary>
		/// Initializes the existing <see cref="Target"/> or new instance of type <see cref="TClient"/> using the provided arguments.
		/// </summary>
		/// <param name="firstArgument"> The first argument to pass to the target's Init function. </param>
		/// <param name="secondArgument"> The second argument to pass to the target's Init function. </param>
		/// <returns> The existing <see cref="target"/> or new instance of type <see cref="TClient"/>. </returns>
		[NotNull]
		protected virtual TClient InitTarget(TFirstArgument firstArgument, TSecondArgument secondArgument)
        {
            if(target == null)
            {
				return target = gameObject.AddComponent<TClient, TFirstArgument, TSecondArgument>(firstArgument, secondArgument);
            }
            
			if(!target.gameObject.scene.IsValid())
            {
				return target.Instantiate(firstArgument, secondArgument);
            }

			target.Init(firstArgument, secondArgument);
			return target;
		}

		#if UNITY_EDITOR
		private void OnValidate()
        {
			bool canBeNull = CanArgumentsBeNullInEditMode(gameObject, nullArgumentGuard);
			Validate(ref firstArgument, this, canBeNull);
			Validate(ref secondArgument, this, canBeNull);
		}
		#endif
    }
}