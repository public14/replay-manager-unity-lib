﻿#pragma warning disable CS0414

using JetBrains.Annotations;
using System;
using UnityEngine;

namespace Sisus.Init
{
	using static Internal.InitializerUtility;
	#if UNITY_EDITOR
	using static EditorOnly.AutoInitUtility;
	#endif

	/// <summary>
	/// A base class for a component that can specify the five arguments used to
	/// initialize an object that implements
	/// <see cref="IInitializable{TFirstArgument, TSecondArgument, TThirdArgument, TFourthArgument, TFifthArgument}"/>.
	/// <para>
	/// The argument values can be assigned using the inspector and serialized as part of a scene or a prefab.
	/// </para>
	/// <para>
	/// The arguments get injected to the <typeparamref name="TClient">client</typeparamref>
	/// during the <see cref="Awake"/> event.
	/// </para>
	/// <para>
	/// The client receives the arguments via the
	/// <see cref="IInitializable{TFirstArgument, TSecondArgument, TThirdArgument, TFourthArgument, TFifthArgument}.Init">Init</see>
	/// method where it can assigned them to member fields or properties.
	/// </para>
	/// <para>
	/// After the arguments have been injected the <see cref="Initializer{,,,,,}"/> is removed from the
	/// <see cref="GameObject"/> that holds it.
	/// </para>
	/// </summary>
	/// <typeparam name="TClient"> Type of the initialized client component. </typeparam>
	/// <typeparam name="TFirstArgument"> Type of the first argument received by the client component. </typeparam>
	/// <typeparam name="TSecondArgument"> Type of the second argument received by the client component. </typeparam>
	/// <typeparam name="TThirdArgument"> Type of the third argument received by the client component. </typeparam>
	/// <typeparam name="TFourthArgument"> Type of the fourth argument received by the client component. </typeparam>
	/// <typeparam name="TFifthArgument"> Type of the fifth argument received by the client component. </typeparam>
	public abstract class Initializer<TClient, TFirstArgument, TSecondArgument, TThirdArgument, TFourthArgument, TFifthArgument> : MonoBehaviour, IInitializer, IValueProvider<TClient>
		where TClient : MonoBehaviour, IInitializable<TFirstArgument, TSecondArgument, TThirdArgument, TFourthArgument, TFifthArgument>
	{
		[SerializeField, Tooltip(TargetTooltip)]
		protected TClient target = null;

		[SerializeField]
		private Any<TFirstArgument> firstArgument = default;
		[SerializeField]
		private Any<TSecondArgument> secondArgument = default;
		[SerializeField]
		private Any<TThirdArgument> thirdArgument = default;
		[SerializeField]
		private Any<TFourthArgument> fourthArgument = default;
		[SerializeField]
		private Any<TFifthArgument> fifthArgument = default;

		[SerializeField, Tooltip(NullArgumentGuardTooltip)]
		private NullArgumentGuard nullArgumentGuard = NullArgumentGuard.EditModeWarning | NullArgumentGuard.RuntimeException;

		/// <inheritdoc/>
		TClient IValueProvider<TClient>.Value => target;

		/// <inheritdoc/>
		object IValueProvider.Value => target;

		/// <inheritdoc/>
		object IInitializer.Target => target;

		/// <inheritdoc/>
		Type IInitializer.TargetType => typeof(TClient);

		/// <inheritdoc/>
		object IInitializer.InitTarget() => InitTarget();

		#if UNITY_EDITOR
        private void Reset()
		{
			OnReset(this, ref target);
			PrepareArgumentsForAutoInit<TClient, TFirstArgument, TSecondArgument, TThirdArgument, TFourthArgument, TFifthArgument>(this);
			ResetArgument(ref firstArgument, 0, true, this);
			ResetArgument(ref secondArgument, 1, true, this);
			ResetArgument(ref thirdArgument, 2, true, this);
			ResetArgument(ref fourthArgument, 3, true, this);
			ResetArgument(ref fifthArgument, 4, true, this);
		}
		#endif

		private void Awake() => InitTarget();

		private TClient InitTarget()
		{
			var firstArgument = this.firstArgument.GetValueOrDefault(this, Context.Awake);
			var secondArgument = this.secondArgument.GetValueOrDefault(this, Context.Awake);
			var thirdArgument = this.thirdArgument.GetValueOrDefault(this, Context.Awake);
			var fourthArgument = this.fourthArgument.GetValueOrDefault(this, Context.Awake);
			var fifthArgument = this.fifthArgument.GetValueOrDefault(this, Context.Awake);

			#if DEBUG || INIT_ARGS_SAFE_MODE
			if(nullArgumentGuard.IsEnabled(NullArgumentGuard.RuntimeException))
			{
				if(firstArgument is null) throw GetMissingInitArgumentsException(GetType(), typeof(TClient), typeof(TFirstArgument));
				if(secondArgument is null) throw GetMissingInitArgumentsException(GetType(), typeof(TClient), typeof(TSecondArgument));
				if(thirdArgument is null) throw GetMissingInitArgumentsException(GetType(), typeof(TClient), typeof(TThirdArgument));
				if(fourthArgument is null) throw GetMissingInitArgumentsException(GetType(), typeof(TClient), typeof(TFourthArgument));
				if(fifthArgument is null) throw GetMissingInitArgumentsException(GetType(), typeof(TClient), typeof(TFifthArgument));
			}
			#endif

			target = InitTarget(firstArgument, secondArgument, thirdArgument, fourthArgument, fifthArgument);

			Destroy(this);

			return target;
		}

		/// <summary>
		/// Initializes the <see cref="target"/> using the provided arguments.
		/// </summary>
		/// <param name="firstArgument"> The first argument to pass to the target's Init function. </param>
		/// <param name="secondArgument"> The second argument to pass to the target's Init function. </param>
		/// <param name="thirdArgument"> The third argument to pass to the target's Init function. </param>
		/// <param name="fourthArgument"> The fourth argument to pass to the target's Init function. </param>
		/// <param name="fifthArgument"> The fifth argument to pass to the target's Init function. </param>
		/// <returns> The existing <see cref="target"/> or new instance of type <see cref="TClient"/>. </returns>
		[NotNull]
		protected virtual TClient InitTarget(TFirstArgument firstArgument, TSecondArgument secondArgument, TThirdArgument thirdArgument, TFourthArgument fourthArgument, TFifthArgument fifthArgument)
        {
            if(target == null)
            {
                return target = gameObject.AddComponent<TClient, TFirstArgument, TSecondArgument, TThirdArgument, TFourthArgument, TFifthArgument>(firstArgument, secondArgument, thirdArgument, fourthArgument, fifthArgument);
            }
            
			if(!target.gameObject.scene.IsValid())
            {
				return target.Instantiate(firstArgument, secondArgument, thirdArgument, fourthArgument, fifthArgument);
            }

			target.Init(firstArgument, secondArgument, thirdArgument, fourthArgument, fifthArgument);
			return target;
        }

		#if UNITY_EDITOR
		private void OnValidate()
        {
			bool canBeNull = CanArgumentsBeNullInEditMode(gameObject, nullArgumentGuard);
			Validate(ref firstArgument, this, canBeNull);
			Validate(ref secondArgument, this, canBeNull);
			Validate(ref thirdArgument, this, canBeNull);
			Validate(ref fourthArgument, this, canBeNull);
			Validate(ref fifthArgument, this, canBeNull);
		}
		#endif
    }
}