﻿using System;
using UnityEngine;
using JetBrains.Annotations;
using System.Runtime.Serialization;
using Object = UnityEngine.Object;

#if UNITY_EDITOR
using System.Reflection;
using UnityEditor;
using UnityEditorInternal;
using static Sisus.Init.ServiceUtility;
using static Sisus.Init.EditorOnly.AutoInitUtility;
using static Sisus.NullExtensions;
using UnityEditor.Experimental.SceneManagement;
#endif

namespace Sisus.Init.Internal
{
	internal static class InitializerUtility
	{
		internal const string TargetTooltip = "Existing target instance to initialize.\n\n" +
		"If value is null then the argument is passed to a new instance that is attached to the " +
		"same GameObject that this initializer is attached to.\n\n" +
		"If value is a prefab then the argument is injected to a new instance of that prefab.";

		internal const string NullArgumentGuardTooltip =
			"Specifies how this object should guard against the argument being null.\n\n" +
				"None:\nAllow the argument to be null in edit mode and to be passed as null at runtime.\n\n" +
				"Edit Mode Warning:\nWarn about the arguments being null in edit mode.\n" +
					"Note that validation in edit mode might not give accurate results if the argument is a service that only becomes available at runtime.\n\n" +
				"Runtime Exception:\nThrow an exception if the argument is null at runtime when it should be injected to the client.\n\n" +
				"Enabled For Prefabs:\nWarn about null arguments detected on prefab assets.\n" +
					"Note that this option only affects prefab assets; prefab instances in scenes will still give warnings about null arguments in Edit Mode if the 'Edit Mode Warning' flag is enabled.";


		internal static TComponent FindClientComponentRelativeTo<TComponent>(GameObject gameObject) where TComponent : Component
		{
			if(gameObject.TryGetComponent(out TComponent target))
			{
				return target;
			}

			return Find.In<TComponent>(gameObject, Including.Children | Including.Parents | Including.Scene | Including.Inactive);
		}

		internal static MissingInitArgumentsException GetMissingInitArgumentsException(Type initializerType, Type clientType, Type argumentType)
		{
			return new MissingInitArgumentsException($"{initializerType.Name} failed to initialize {clientType.Name} because missing argument of type {argumentType.Name}. Assign a reference using the Inspector or add the [Service(typeof({argumentType.Name}))] attribute to a class that derives from {argumentType.Name}.");
		}

		#if UNITY_EDITOR
		internal static void ResetArgument<TClient, TArgument>(ref Any<TArgument> argument)
        {
			ResetArgument<MonoBehaviour, TArgument>(ref argument, -1, false, null);
		}

		internal static void ResetArgument<TClient, TArgument>(ref Any<TArgument> argument, int argumentIndex, bool canAutoInitArguments, TClient autoInitFrom) where TClient : MonoBehaviour
		{
			if(IsServiceType<TArgument>())
			{
				return;
			}

			if(canAutoInitArguments && autoInitFrom != null)
            {
				var value = GetAutoInitArgument<TClient, TArgument>(autoInitFrom, argumentIndex) as Object;
				if(Any<TArgument>.IsCreatableFrom(value))
				{
					argument = new Any<TArgument>();
				}
			}

			if(typeof(TArgument).IsValueType)
            {
				argument = new Any<TArgument>(default(TArgument));
				return;
            }

			if(argument.HasValue || typeof(Object).IsAssignableFrom(typeof(TArgument)) || typeof(Type).IsAssignableFrom(typeof(TArgument)))
			{
				return;
			}

			if(!typeof(TArgument).IsAbstract)
			{
				argument = CreateInstance<TArgument>();
				return;
			}

			Type onlyConcreteType = null;
			foreach(Type derivedType in TypeUtility.GetDerivedTypes<TArgument>())
			{
				if(derivedType.IsAbstract)
                {
					continue;
                }

				if(onlyConcreteType != null)
                {
					return;
                }

				onlyConcreteType = derivedType;
			}

			if(onlyConcreteType is null || typeof(Object).IsAssignableFrom(onlyConcreteType) || typeof(Type).IsAssignableFrom(onlyConcreteType))
            {
				return;
            }

			argument = (TArgument)CreateInstance(onlyConcreteType);
		}

		internal static T CreateInstance<T>() => (T)CreateInstance(typeof(T));

		internal static object CreateInstance(Type type)
        {
			if(typeof(Type).IsAssignableFrom(type))
            {
				#if DEV_MODE
				Debug.LogWarning($"Can not create instance of a {type.FullName} using FormatterServices.GetUninitializedObject.");
				#endif
				return default;
            }

			try
			{
				return Activator.CreateInstance(type);
			}
			catch(Exception)
			{
				return FormatterServices.GetUninitializedObject(type);
			}
        }

		internal static void OnReset<TClient>([NotNull] Component initializer, ref TClient client) where TClient : Component
        {
            client = FindClientComponentRelativeTo<TClient>(initializer.gameObject);

            if(client == null || client.gameObject != initializer.gameObject)
            {
                return;
            }

			MoveInitializerAboveItsClient(initializer, client);
        }

        private static void MoveInitializerAboveItsClient([NotNull] Component initializer, [NotNull] Component target)
        {
			var moveComponentRelativeToComponent = typeof(ComponentUtility).GetMethod("MoveComponentRelativeToComponent", BindingFlags.Static | BindingFlags.NonPublic, null, new Type[] { typeof(Component), typeof(Component), typeof(bool) }, null);
			if(moveComponentRelativeToComponent != null)
			{
				moveComponentRelativeToComponent.Invoke(null, new object[] { initializer, target, true });
			}
			else
			{
				while(ComponentUtility.MoveComponentUp(initializer));
			}
		}

		internal static void Validate<TArgument>(Object argumentReference, ref TArgument argumentValue, Component context, bool canBeNull)
        {
			if(argumentValue is Object)
            {
				argumentValue = default;
            }

			if(canBeNull || argumentReference != null || !ReferenceEquals(argumentValue, null) || IsServiceType<TArgument>() || Array.IndexOf(Selection.gameObjects, context.gameObject) != -1)
            {
				return;
            }

			Debug.LogWarning($"{context.GetType().Name} on GameObject \"{context.name}\" is missing argument of type {typeof(TArgument).Name}.\nIf the argument is allowed to be null set the 'Null Argument Guard' option to 'None'.\nIf the argument is a service that only becomes available at runtime set the option to 'Runtime Exception'.", context);
		}

		internal static void Validate<TArgument>(ref Any<TArgument> argument, Component context, bool canBeNull)
        {
			if(IsServiceType<TArgument>())
            {
				if(argument.HasSerializedValue())
				{
					Debug.LogWarning($"Resetting argument of type {argument.GetType().Name} of {context.GetType().Name} on GameObject \"{context.name}\" to default value because it is a global Service type.");
					EditorUtility.SetDirty(context);
					argument = default;
				}
				return;
            }

			if(canBeNull || argument.GetHasValue(context, Context.MainThread) || Array.IndexOf(Selection.gameObjects, context.gameObject) != -1)
            {
				return;
            }

			Debug.LogWarning($"{context.GetType().Name} on GameObject \"{context.name}\" is missing argument of type {typeof(TArgument).Name}.\nIf the argument is allowed to be null set the 'Null Argument Guard' option to 'None'.\nIf the argument is a service that only becomes available at runtime set the option to 'Runtime Exception'.", context);
		}

		internal static bool CanArgumentsBeNullInEditMode(GameObject gameObject, NullArgumentGuard nullArgumentGuard) => nullArgumentGuard.IsDisabled(NullArgumentGuard.EditModeWarning)
				|| (nullArgumentGuard.IsDisabled(NullArgumentGuard.EnabledForPrefabs) && (PrefabUtility.IsPartOfPrefabAsset(gameObject) || PrefabStageUtility.GetPrefabStage(gameObject) != null));

		#endif
	}
}