﻿#pragma warning disable CS0414

using System;
using JetBrains.Annotations;
using UnityEngine;

namespace Sisus.Init
{
	using static Internal.InitializerUtility;
	#if UNITY_EDITOR
	using static EditorOnly.AutoInitUtility;
	#endif

	/// <summary>
	/// A base class for a component that can specify the argument used to initialize an
	/// object which then gets wrapped by a <see cref="Wrapper{TWrapped}"/> component.
	/// <para>
	/// The argument value can be assigned using the inspector and serialized as part of a scene or a prefab.
	/// </para>
	/// <para>
	/// The <typeparamref name="TWrapped">wrapped object</typeparamref> gets created and injected to
	/// the <typeparamref name="TWrapper">wrapper component</typeparamref> during the <see cref="Awake"/> event.
	/// </para>
	/// <para>
	/// After the object has been injected the <see cref="WrapperInitializer{,,}"/> is removed from the
	/// <see cref="GameObject"/> that holds it.
	/// </para>
	/// </summary>
	/// <typeparam name="TWrapper"> Type of the initialized wrapper component. </typeparam>
	/// <typeparam name="TWrapped"> Type of the object wrapped by the wrapper. </typeparam>
	/// <typeparam name="TArgument"> Type of the first argument received by the wrapped object. </typeparam>
	public abstract class WrapperInitializer<TWrapper, TWrapped, TArgument>
		: MonoBehaviour, IInitializer, IValueProvider<TWrapped> where TWrapper : Wrapper<TWrapped>
	{
		[SerializeField]
		protected TWrapper target = null;

		[SerializeField]
		private Any<TArgument> argument = default;

		[SerializeField, Tooltip(NullArgumentGuardTooltip)]
		private NullArgumentGuard nullArgumentGuard = NullArgumentGuard.EditModeWarning | NullArgumentGuard.RuntimeException;

		/// <inheritdoc/>
		TWrapped IValueProvider<TWrapped>.Value => target != null ? target.WrappedObject : default;

		/// <inheritdoc/>
		object IValueProvider.Value => target != null ? target.WrappedObject : default(TWrapped);

		/// <inheritdoc/>
		object IInitializer.Target => target != null ? target.WrappedObject : default(TWrapped);

		/// <inheritdoc/>
		Type IInitializer.TargetType => typeof(TWrapped);

		/// <inheritdoc/>
		object IInitializer.InitTarget() => InitTarget();

		#if UNITY_EDITOR
        private void Reset()
		{
			OnReset(this, ref target);
			PrepareArgumentForAutoInit<TWrapper, TArgument>(this);

			if(target != null)
			{
				ResetArgument(ref argument, 0, true, target);
			}
			else
			{
				ResetArgument(ref argument, 0, true, this);
			}
		}
		#endif

		private void Awake() => InitTarget();

		private TWrapped InitTarget()
		{
			var argument = this.argument.ValueOrDefault;

			#if DEBUG || INIT_ARGS_SAFE_MODE
			if(argument is null && nullArgumentGuard.IsEnabled(NullArgumentGuard.RuntimeException))
			{
				throw GetMissingInitArgumentsException(GetType(), typeof(TWrapper), typeof(TArgument));
			}
			#endif

			var wrappedObject = CreateWrappedObject(argument);
			var wrapper = InitWrapper(wrappedObject);

			Destroy(this);

			return wrappedObject;
		}

		/// <summary>
		/// Returns a new instance of <see cref="TWrapped"/> initializes using the provided arguments.
		/// </summary>
		/// <param name="argument"> The argument used to initialize the wrapped object. </param>
		/// <returns> Instance of the <see cref="TWrapped"/> class. </returns>
		[NotNull]
		protected abstract TWrapped CreateWrappedObject(TArgument argument);

		/// <summary>
		/// Intializes the <see cref="target"/> <see cref="TWrapper"/> with the provided <paramref name="wrappedObject">wrapped object</paramref>.
		/// </summary>
		/// <param name="wrappedObject"> The <see cref="TWrapped">wrapped object</see> to pass to the <see cref="target"/> <typeparamref name="TWrapper">wrapper</typeparamref>'s Init function. </param>
		/// <returns> The existing <see cref="target"/> or new instance of type <see cref="TWrapper"/>. </returns>
		[NotNull]
		protected virtual TWrapper InitWrapper(TWrapped wrappedObject)
        {
            if(target == null)
            {
				return gameObject.AddComponent<TWrapper, TWrapped>(wrappedObject);
            }

            if(!target.gameObject.scene.IsValid())
            {
				return target.Instantiate(wrappedObject);
            }

            (target as IInitializable<TWrapped>).Init(wrappedObject);
			return target;
		}

		#if UNITY_EDITOR
        private void OnValidate()
		{
			bool canBeNull = CanArgumentsBeNullInEditMode(gameObject, nullArgumentGuard);
			Validate(ref argument, this, canBeNull);
		}
		#endif
    }
}