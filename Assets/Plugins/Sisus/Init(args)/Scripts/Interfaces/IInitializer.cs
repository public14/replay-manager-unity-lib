﻿using JetBrains.Annotations;
using System;
using Object = UnityEngine.Object;

namespace Sisus.Init
{
	/// <summary>
	/// Represents an object that can can specify the arguments used to
	/// <see cref="IInitializable{}.Init">initialize</see> objects
	/// that implement one of the <see cref="IInitializable{}"/> interfaces.
	/// <para>
	/// Implemented by all the <see cref="Initializer{,}">Initializer</see> base classes.
	/// </para>
	/// </summary>
	public interface IInitializer
	{
		/// <summary>
		/// Existing target instance to initialize, if any.
		/// <para>
		/// If value is <see langword="null"/> then the argument is injected to a new instance.
		/// </para>
		/// <para>
		/// If value is a prefab then the argument is injected to a new instance of that prefab.
		/// </para>
		/// </summary>
		[CanBeNull]
		object Target { get; }

		/// <summary>
		/// Type of the class that is getting initialized.
		/// </summary>
		Type TargetType { get; }

		/// <summary>
		/// Initializes the target with the init arguments specified on this initializer.
		/// </summary>
		/// <returns> The existing <see cref="Target"/> or new instance of type <see cref="TargetType"/>. </returns>
		[NotNull]
		object InitTarget();
	}
}