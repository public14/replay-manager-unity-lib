﻿//#define DEBUG_INIT_SERVICES
#define DEBUG_CREATE_SERVICES

using System;
using System.Reflection;
using UnityEngine;
using JetBrains.Annotations;
using Sisus.Init.Internal;
using UnityEngine.Scripting;

namespace Sisus.Init
{
    /// <summary>
    /// Utility class responsible for providing information about <see cref="ServiceAttribute">services</see>.
    /// </summary>
    public static class ServiceUtility
    {
        private static readonly MethodInfo serviceGetForAnyMethodDefinition;
        private static readonly MethodInfo serviceGetForClientMethodDefinition;
        private static readonly MethodInfo serviceExistsForClientMethodDefinition;
        private static readonly MethodInfo setInstanceMethodDefinition;

        static ServiceUtility()
        {
            const BindingFlags flags = BindingFlags.Static | BindingFlags.Public;
            var objectParameterType = new Type[] { typeof(object) };
            serviceGetForAnyMethodDefinition = typeof(Service).GetMethod(nameof(Service.Get), BindingFlags.Static | BindingFlags.Public, null, Type.EmptyTypes, null);
            serviceGetForClientMethodDefinition = typeof(Service).GetMethod(nameof(Service.Get), flags, null, objectParameterType, null);
            serviceExistsForClientMethodDefinition = typeof(Service).GetMethod(nameof(Service.Exists), flags, null, objectParameterType, null);
            setInstanceMethodDefinition = typeof(Service).GetMethod(nameof(Service.SetInstance), flags);
        }

        #if UNITY_EDITOR
        private static bool isPlaying = true;
        #endif

        /// <summary>
        /// <see langword="true"/> if all <see cref="ServiceAttribute">services</see>
        /// have been created, initialized and are ready to be used by clients.
        /// </summary>
        public static bool ServicesAreReady
        {
            get
            {
                #if INIT_ARGS_DISABLE_SERVICE_INJECTION
                return true;
                #else
                
                #if UNITY_EDITOR
                if(!isPlaying)
                {
                    return EditorServiceInjector.ServicesAreReady;
                }
                #endif


                return ServiceInjector.ServicesAreReady;
                #endif
            }
        }

        /// <summary>
        /// Event that is broadcast when all <see cref="ServiceAttribute">services</see> have been created,
        /// initialized and are ready to be used by clients.
        /// </summary>
        public static event Action OnServicesBecameReady
        {
            add
            {
                #if INIT_ARGS_DISABLE_SERVICE_INJECTION
                value?.Invoke();
                #else
                if(ServiceInjector.ServicesAreReady)
                {
                    value?.Invoke();
                }

                ServiceInjector.OnServicesBecameReady += value;
                #endif
            }

            remove
            {
                #if !INIT_ARGS_DISABLE_SERVICE_INJECTION
                ServiceInjector.OnServicesBecameReady -= value;
                #endif
            }
        }

        /// <summary>
        /// Gets the shared service instance of the given <paramref name="serviceType"/>.
        /// <para>
        /// The returned object's class will match the provided <paramref name="serviceType"/>,
        /// derive from it or implement an interface of the type.
        /// </para>
        /// <para>
        /// If no such service has been registered then <see langword="null"/> is returned.
        /// </para>
        /// </summary>
        [Preserve]
        public static object GetService([NotNull] Type serviceType)
        {
            if(serviceType.IsValueType)
            {
                return null;
            }

            return serviceGetForAnyMethodDefinition.MakeGenericMethod(serviceType).Invoke(null, null);
        }

        /// <summary>
        /// Gets the shared service instance of the given <paramref name="serviceType"/>.
        /// <para>
        /// The returned object's class will match the provided <paramref name="serviceType"/>,
        /// derive from it or implement an interface of the type.
        /// </para>
        /// <para>
        /// If no such service has been registered then <see langword="null"/> is returned.
        /// </para>
        /// </summary>
        [Preserve]
        public static object GetService(object client, [NotNull] Type serviceType)
        {
            if(serviceType.IsValueType)
            {
                return null;
            }

            return serviceGetForClientMethodDefinition.MakeGenericMethod(serviceType).Invoke(null, new object[] { client });
        }

        /// <summary>
        /// Determines whether or not service of type <typeparamref name="TService"/>
        /// is available for the <paramref name="client"/>.
        /// <para>
        /// The service can be located from <see cref="Services"/> components in the active scenes,
        /// or failing that, from the globally shared <see cref="Service{TService}.Instance"/>.
        /// </para>
        /// <para>
        /// This method can only be called from the main thread.
        /// </para>
        /// </summary>
        /// <param name="client"> The client that needs the service. </param>
        /// <param name="client"> The defining type of the service. </param>
        /// <returns>
        /// <see langword="true"/> if service of the given type exists for the client; otherwise, <see langword="false"/>.
        /// </returns>
        [Preserve]
        public static bool ServiceExists(object client, [NotNull] Type serviceType)
        {
            if(serviceType.IsValueType)
            {
                return false;
            }

            return (bool)serviceExistsForClientMethodDefinition.MakeGenericMethod(serviceType).Invoke(null, new object[] { client });
        }

        /// <summary>
        /// Sets the <see cref="Service{}.Instance">service instance</see> of the provided
        /// <paramref name="definingType">type</paramref> that is shared across clients
        /// to the given value.
        /// <para>
        /// If the provided instance is not equal to the old <see cref="Service{}.Instance"/>
        /// then the <see cref="Service{}.InstanceChanged"/> event will be raised.
        /// </para>
        /// </summary>
        /// <param name="instance"> The new instance of the service. </param>
        [Preserve]
        public static void SetInstance([NotNull] Type definingType, [NotNull] object instance)
        {
            Debug.Assert(definingType != null);
            Debug.Assert(instance != null);

            if(!definingType.IsAssignableFrom(instance.GetType()))
            {
                if(definingType.IsInterface)
                {
                    Debug.LogWarning($"Invalid Service Definition: Service '{instance.GetType().Name}' has the {nameof(ServiceAttribute)} with defining interface type {definingType.Name} but it does not implement {definingType.Name}.");
                }
                else
                {
                    Debug.LogWarning($"Invalid Service Definition: Service '{instance.GetType().Name}' has the {nameof(ServiceAttribute)} with defining type {definingType.Name} but {definingType.Name} is not a derived type.");
                }
                return;
            }

            var setInstanceMethodDefinition = typeof(Service).GetMethod(nameof(Service.SetInstance), BindingFlags.Static | BindingFlags.Public);
            var setInstanceMethod = setInstanceMethodDefinition.MakeGenericMethod(definingType);
            setInstanceMethod.Invoke(null, new object[] { instance });
        }

        /// <summary>
        /// Gets a value indicating whether or not <typeparamref name="T"/> is a service type.
        /// <para>
        /// Service types are non-abstract classes that have the <see cref="ServiceAttribute"/>.
        /// </para>
        /// </summary>
        /// <typeparam name="T"> Type to test. </typeparam>
        /// <returns> <see langword="true"/> if <typeparamref name="T"/> is a concrete service type; otherwise, <see langword="false"/>. </returns>
        public static bool IsServiceType<T>()
        {
            #if UNITY_EDITOR && !INIT_ARGS_DISABLE_SERVICE_INJECTION
            return ServiceInjector.IsServiceType<T>();
            #else
            return !typeof(T).IsValueType && Service<T>.Instance != null;
            #endif
        }

        /// <summary>
        /// Gets a value indicating whether or not <typeparamref name="T"/> is a service type.
        /// <para>
        /// Service types are non-abstract classes that have the <see cref="ServiceAttribute"/>.
        /// </para>
        /// </summary>
        /// <param name="type"> Type to test. </param>
        /// <returns> <see langword="true"/> if <typeparamref name="T"/> is a concrete service type; otherwise, <see langword="false"/>. </returns>
        public static bool IsServiceType([NotNull] Type type)
        {
            if(type.IsValueType)
            {
                return false;
            }

            #if UNITY_EDITOR && !INIT_ARGS_DISABLE_SERVICE_INJECTION
            if(type is null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            return ServiceInjector.IsServiceType(type);
            #elif DEBUG
            return !(typeof(Service<>).MakeGenericType(type).GetProperty(nameof(Service<object>.Instance), BindingFlags.Static | BindingFlags.Public).GetValue(null) is null);
            #else
            return !(typeof(Service<>).MakeGenericType(type).GetField(nameof(Service<object>.Instance), BindingFlags.Static | BindingFlags.Public).GetValue(null) is null);
            #endif
        }
    }
}