﻿using JetBrains.Annotations;
using Sisus.Init.Internal;
using System;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using System.Linq;
#endif

namespace Sisus.Init
{
    using static ConversionExtensions;
    using static NullExtensions;
    using Object = UnityEngine.Object;

    public partial class Services
    {
        private static readonly Dictionary<Type, List<ServiceInfo>> infosByType = new Dictionary<Type, List<ServiceInfo>>();
        
        #if UNITY_EDITOR
        private static readonly Dictionary<Type, List<ServiceInfo>> infosByTypeInEditMode = new Dictionary<Type, List<ServiceInfo>>();
        #endif

        internal static bool TryGetForAnyClient<TService>(out TService service)
        {
            #if UNITY_EDITOR
            var infosByType = EditorOnly.ThreadSafe.Application.IsPlaying ? Services.infosByType : infosByTypeInEditMode;
            #endif

            if(infosByType.TryGetValue(typeof(TService), out var infos))
            {
                ServiceInfo nearest = default;

                foreach(var info in infos)
                {
                    #if UNITY_EDITOR
                    if(info.container == null)
                    {
                        continue;
                    }
                    #endif

                    if(info.container.toClients != Clients.Everywhere)
                    {
                        continue;
                    }

                    #if DEBUG && !INIT_ARGS_DISABLE_WARNINGS
                    if(nearest.service != Null)
                    {
                        Debug.LogWarning($"AmbiguousMatchWarning: All clients have access to both services \"{nearest.container.gameObject.name}\"/{TypeUtility.ToString(nearest.service.GetType())} and \"{info.container.gameObject.name}\"/{TypeUtility.ToString(info.service.GetType())} via {nameof(Services)}s and unable to determine which one should be prioritized.");
                    }
                    #endif
                        
                    nearest = info;

                    #if !DEBUG
                    break;
                    #endif
                }

                if(nearest.service != Null && TryConvert(nearest.service, out TService asService))
                {
                    service = asService;
                    return true;
                }
            }

            service = Service<TService>.Instance;
            return !(service is null);
        }
        
        internal static bool TryGetFor<TService>([NotNull] GameObject gameObject, out TService service)
        {
            Debug.Assert(gameObject != null);

            #if UNITY_EDITOR
            var infosByType = EditorOnly.ThreadSafe.Application.IsPlaying ? Services.infosByType : infosByTypeInEditMode;
            #endif

            if(infosByType.TryGetValue(typeof(TService), out var infos))
            {
                ServiceInfo? nearest = default;

                foreach(var info in infos)
                {
                    #if UNITY_EDITOR
                    if(info.container == null)
                    {
                        continue;
                    }
                    #endif

                    if(!info.container.AreAvailableToClient(gameObject))
                    {
                        continue;
                    }

                    nearest = GetNearest(gameObject, info, nearest);
                }

                if(nearest.HasValue && nearest.Value.service is object obj && TryConvert(obj, out TService asService))
                {
                    service = asService;
                    return true;
                }
            }

            if(typeof(TService).IsValueType)
            {
                service = default;
                return false;
            }

            var globalService = Service<TService>.Instance;
            if(globalService != Null)
            {
                service = globalService;
                return true;
            }

            service = default;
            return false;
        }

        private static void Register([NotNull] Services services)
        {
            Debug.Assert(services != null);

            #if UNITY_EDITOR
            var infosByType = EditorOnly.ThreadSafe.Application.IsPlaying ? Services.infosByType : infosByTypeInEditMode;
            #endif

            foreach(var serviceDefinition in services.providesServices)
            {
                if(serviceDefinition.service == null)
                {
                    Debug.LogWarning($"{nameof(Services)} component has a missing service reference.", services);
                    continue;
                }

                var definingType = serviceDefinition.definingType?.Value;
                if(definingType is null)
                {
                    Debug.LogWarning($"{nameof(Services)} component service {serviceDefinition.service.GetType().Name} is missing its defining type.", services);
                    continue;
                }

                if(!infosByType.TryGetValue(definingType, out var infos))
                {
                    infos = new List<ServiceInfo>();
                    infosByType.Add(definingType, infos);
                }
                #if UNITY_EDITOR
                else if(infos.Any(i => i.service as Object == serviceDefinition.service && i.container == services))
                {
                    continue;
                }
                #endif

                infos.Add(new ServiceInfo(serviceDefinition.service, services));
            }
        }

        private static void Deregister([NotNull] Services services)
        {
            #if UNITY_EDITOR
            var infosByType = EditorOnly.ThreadSafe.Application.IsPlaying ? Services.infosByType : infosByTypeInEditMode;
            #endif

            foreach(var instance in infosByType)
            {
                var infos = instance.Value;
                for(int i = infos.Count - 1; i >= 0; i--)
                {
                    if(infos[i].container == services)
                    {
                        infos.RemoveAt(i);
                    }
                }
            }
        }

        private static ServiceInfo GetNearest([NotNull] GameObject client, ServiceInfo firstOption, ServiceInfo? secondOptionOrNull)
        {
            if(!secondOptionOrNull.HasValue)
            {
                return firstOption;
            }

            var firstService = firstOption.service;
            var secondOption = secondOptionOrNull.Value;
            var secondService = secondOption.service;

            if(firstService is null)
            {
                return secondOption;
            }

            if(secondService is null)
            {
                return firstOption;
            }

            if(ReferenceEquals(firstService, secondService))
            {
                return firstOption;
            }

            var scene = client.gameObject.scene;

            if(firstOption.container.gameObject.scene != scene)
            {
                #if DEBUG
                if(secondOption.container.gameObject.scene != scene)
                {
                    Debug.LogWarning($"AmbiguousMatchWarning: Client on GameObject \"{client.name}\" has access to both services {TypeUtility.ToString(firstOption.service.GetType())} and {TypeUtility.ToString(secondOption.service.GetType())} via {nameof(Services)}s and unable to determine which one should be prioritized.", client);
                }
                #endif

                return secondOption;
            }

            if(secondOption.container.gameObject.scene != scene)
            {
                return firstOption;
            }

            var firstTransform = firstOption.container.gameObject.transform;
            var secondTransform = secondOption.container.gameObject.transform;
            
            for(var parent = client.transform; parent != null; parent = parent.parent)
            {
                if(parent == firstTransform)
                {
                    if(parent == secondTransform)
                    {
                        break;
                    }

                    return firstOption;
                }

                if(parent == secondTransform)
                {
                    return secondOption;
                }
            }

            #if DEBUG
            Debug.LogWarning($"AmbiguousMatchWarning: Client on GameObject \"{client.name}\" has access to both services \"{firstOption.container.gameObject.name}\"/{TypeUtility.ToString(firstOption.service.GetType())} and \"{secondOption.container.gameObject.name}\"/{TypeUtility.ToString(secondOption.service.GetType())} via {nameof(Services)}s and unable to determine which one should be prioritized.", client);
            #endif

            return firstOption;
        }

        private readonly struct ServiceInfo
        {
            public readonly object service;
            public readonly Services container;

            public ServiceInfo(object service, Services container)
            {
                this.service = service;
                this.container = container;
            }
        }
    }
}