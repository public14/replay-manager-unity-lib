using System;
using JetBrains.Annotations;
using UnityEngine;

namespace Sisus.Init
{
	/// <summary>
	/// Extensions methods for <see cref="GameObject"/> that can be used to wrap plain old class objects with
	/// <see cref="Wrapper{}"/> components and attach them to GameObjects.
	/// </summary>
	public static class WrapperGameObjectExtensions
	{
		/// <summary>
		/// Wraps the plain old class object of type <typeparamref name="TWrapped"/> in a <see cref="IWrapper{TWrapped}">wrapper component</see>
		/// and adds it to the <paramref name="gameObject"/>.
		/// </summary>
		/// <typeparam name="TWrapped"> The type of the object to wrap and add to the <paramref name="gameObject"/>. </typeparam>
		/// <param name="gameObject"> The <see cref="GameObject"/> to which the object is added. </param>
		/// <param name="wrapped"> The object to wrap add to the <paramref name="gameObject"/>. </param>
		/// <returns> The wrapper that was added to the <paramref name="gameObject"/> and was made to wrap the <paramref name="wrapped"/> object. </returns>
		/// <exception cref="ArgumentNullException">
		/// Thrown if <see cref="this"/> <see cref="GameObject"/> or the <paramref name="wrapped"/> is <see langword="null"/>. 
		/// </exception>
		public static IWrapper AddComponent<TWrapped>([NotNull] this GameObject gameObject, TWrapped wrapped)
		{
			#if DEBUG
			if(wrapped is null)
            {
				if(gameObject == null)
				{
					throw new ArgumentNullException($"The wrapped object which you want to add to the GameObject is null.");
				}
				throw new ArgumentNullException($"The wrapped object which you want to add to the GameObject \"{gameObject.name}\" is null.");
			}
			if(gameObject == null)
			{
				throw new ArgumentNullException($"The GameObject to which you want to add the wrapped object {wrapped.GetType().Name} is null.");
			}
			#endif

			var wrappedType = wrapped.GetType();

			if(Find.typeToWrapperTypes.TryGetValue(wrappedType, out var wrapperTypes))
			{
				var wrapperType = wrapperTypes[0];

				InitArgs.Set(wrapperType, wrapped);
				var wrapper = gameObject.AddComponent(wrapperType) as IWrapper<TWrapped>;

				if(InitArgs.Clear<TWrapped>(wrapperType))
				{
					wrapper.Init(wrapped);
				}

				return wrapper;
			}
			else
            {
				var wrapperType = typeof(DefaultWrapper);

				InitArgs.Set<DefaultWrapper, object>(wrapped);
				var wrapper = gameObject.AddComponent<DefaultWrapper>();

				if(InitArgs.Clear<TWrapped>(wrapperType))
				{
					((IInitializable<object>)wrapper).Init(wrapped);
				}

				return wrapper;
			}
		}

		[CanBeNull]
        public static TWrapper Add<TWrapper, TWrapped>([NotNull] this GameObject gameObject)
            where TWrapper : MonoBehaviour, IWrapper<TWrapped> where TWrapped : class, new()
        {
            return gameObject.AddComponent<TWrapper, TWrapped>(new TWrapped());
        }
    }
}