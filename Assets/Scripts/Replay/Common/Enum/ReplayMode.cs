﻿namespace Replay.Common.Enum
{
    /// <summary> 리플레이 모드 </summary>
    public enum ReplayMode
    {
        /// <summary> 저장 </summary>
        Save,

        /// <summary> 불러오기 </summary>
        Load
    }
}