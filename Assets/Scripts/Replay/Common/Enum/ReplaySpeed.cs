﻿namespace Replay.Common.Enum
{
    /// <summary> 리플레이 재생 속도 </summary>
    public enum ReplaySpeed
    {
        /// <summary> 0.5배속 </summary>
        Half,
        /// <summary> 1배속 </summary>
        Origin,
        /// <summary> 2배속 </summary>
        Double,
        /// <summary> 4배속 </summary>
        FourTimes
    }
}