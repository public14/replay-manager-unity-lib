﻿namespace Replay.Common.Enum
{
    /// <summary> 저장 데이터의 단위 </summary>
    public enum SaveDataUnit
    {
        /// <summary> 게임오브젝트를 통째로 저장하는 방식 - 모든 데이터를 보관할 수 있지만, 무겁다. </summary>
        FullGameObjectData,
        
        /// <summary> 게임오브젝트 데이터 중에서 일부만 뽑아서 저장하는 방식 - 가볍지만, 귀찮다. </summary>
        CustomGameObjectData
    }
}