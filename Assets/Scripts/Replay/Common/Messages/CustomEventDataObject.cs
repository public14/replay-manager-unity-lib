﻿namespace Replay.Common.Messages
{
    /// <summary> 이벤트 데이터 전송을 위한 객체 </summary>
    public class CustomEventDataObject
    {
        /// <summary> 프리팹 키 </summary>
        public string PrefabKey { get; }
        
        /// <summary> 게임오브젝트 이름 </summary>
        public string GameObjectName { get; }
        
        /// <summary>
        /// 초기화
        /// </summary>
        /// <param name="prefabKey"></param>
        /// <param name="gameObjectName"></param>
        public CustomEventDataObject(string prefabKey, string gameObjectName)
        {
            this.PrefabKey = prefabKey;
            this.GameObjectName = gameObjectName;
        }
    }
}