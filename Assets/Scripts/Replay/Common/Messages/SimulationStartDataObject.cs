﻿namespace Replay.Common.Messages
{
    /// <summary>
    /// 시뮬레이션 시작 이벤트 데이터 전달 객체
    /// </summary>
    public class SimulationStartDataObject
    {
        /// <summary> 시뮬레이션 Play Id </summary>
        public string PlayId { get; } // 시뮬레이션 플레이 ID

        /// <summary> 시작시간 (UnixTime) </summary>
        public long StartUnixTime { get; }
        
        /// <summary>
        /// 초기화
        /// </summary>
        /// <param name="playId"></param>
        /// <param name="startUnixTime"></param>
        public SimulationStartDataObject(string playId, long startUnixTime)
        {
            this.PlayId = playId;
            this.StartUnixTime = startUnixTime;
        }
    }
}