﻿using System.Collections.Generic;
using MEC;
using Replay.Common.Static;
using UnityEngine;
using UnityEngine.Events;

namespace Replay
{
    /// <summary>
    /// 리플레이용 코루틴 추상 클래스
    /// </summary>
    public abstract class ReplayCoroutine
    {
        /// <summary>
        /// 코루틴 대기 후 실행할 이벤트
        /// </summary>
        public UnityAction<int> InvokeAction;
        
        /// <summary>
        /// 코루틴 실행 추상 메서드
        /// </summary>
        /// <returns></returns>
        public abstract IEnumerator<float> OnExecute();
    }

    /// <summary>
    /// 프레임별로 저장하는 코루틴 구현 클래스
    /// </summary>
    public class ReplayCoroutineByFrame : ReplayCoroutine
    {
        /// <summary>
        /// 코루틴 실행
        /// </summary>
        /// <returns></returns>
        public override IEnumerator<float> OnExecute()
        {
            while (true)
            {
                yield return Timing.WaitForOneFrame; // 1프레임 대기
                InvokeAction.Invoke(Time.frameCount); // 인보크 이벤트 실행
            }
            // ReSharper disable once IteratorNeverReturns
        }
    }
    
    /// <summary>
    /// 일정 시간 간격으로 저장하는 코루틴 구현 클래스
    /// </summary>
    public class ReplayCoroutineByTimeInterval : ReplayCoroutine
    {
        private float _timeInterval;
        private bool _isCentral = false;
        
        /// <summary>
        /// 초기화
        /// </summary>
        /// <param name="savingTimeInterval"></param>
        /// <param name="isCentral"></param>
        public ReplayCoroutineByTimeInterval(float savingTimeInterval, bool isCentral) 
        {
            this._timeInterval = savingTimeInterval;
            this._isCentral = isCentral;
        }

        /// <summary>
        /// 코루틴 실행
        /// </summary>
        /// <returns></returns>
        public override IEnumerator<float> OnExecute()
        {
            while (true)
            {
                yield return Timing.WaitForSeconds(_timeInterval); // 한 간격 대기
                InvokeAction.Invoke(GlobalStaticParams.GlobalSequence); // 인보크 이벤트 실행
                if (this._isCentral)
                {
                    // 리플레이 중앙 관리자(ReplayCentralManager)가 실행한 코루틴인 경우에 전역 시퀀스를 1씩 상승시킨다.  
                    GlobalStaticParams.GlobalSequence++;
                }
            }
            // ReSharper disable once IteratorNeverReturns
        }
    }
}