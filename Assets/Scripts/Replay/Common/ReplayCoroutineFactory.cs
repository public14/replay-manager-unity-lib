﻿using Replay.Common.Enum;
using Replay.Common.Static;
using UnityEngine;

namespace Replay
{
    /// <summary>
    /// ReplayCoroutine 생성을 담당하는 팩토리 
    /// </summary>
    public static class ReplayCoroutineFactory
    {
        /// <summary>
        /// ReplayCoroutine 코루틴을 생성하는 팩토리 메서드
        /// </summary>
        /// <param name="replayMode"></param>
        /// <param name="isCentral"></param>
        /// <returns></returns>
        public static ReplayCoroutine CreateCoroutine(ReplayMode replayMode, bool isCentral)
        {
            ReplayCoroutine replayCoroutine;
            float coroutineInterval = CreateCoroutineInterval(replayMode);
            
            if (coroutineInterval > 0)
            {
                // 간격이 0보다 크면, 해당 간격대로 Coroutine을 생성한다.
                replayCoroutine = new ReplayCoroutineByTimeInterval(coroutineInterval, isCentral);
            }
            else if (coroutineInterval.Equals(0))
            {
                // 만약에 간격이 0이면, Frame 별로 저장하라는 의미
                replayCoroutine = new ReplayCoroutineByFrame();
            }
            else
            {
                replayCoroutine = null;
                Debug.LogError(GlobalLogMessages.MESSAGE_WRONG_INPUT);
            }
            return replayCoroutine;
        }
        
        /// <summary>
        /// 코루틴 실행 간격(딜레이)를 생성하는 메서드
        /// </summary>
        /// <param name="replayMode"></param>
        /// <returns></returns>
        private static float CreateCoroutineInterval(ReplayMode replayMode)
        {
            var globalSavingTimeInterval = ReplayManager.Instance().savingTimeInterval;
            var globalReplaySpeed = ReplayManager.Instance().replaySpeed;
            
            Debug.Log($"globalSavingTimeInterval : {globalSavingTimeInterval}, globalReplaySpeed: {globalReplaySpeed}");

            if (replayMode == ReplayMode.Save)
            {
                return globalSavingTimeInterval;
            }
            else
            {
                float result = globalSavingTimeInterval;
                
                switch (globalReplaySpeed)
                {
                    case ReplaySpeed.Half:
                        result = globalSavingTimeInterval * 2f;
                        break;
                
                    case ReplaySpeed.Origin:
                        result = globalSavingTimeInterval;
                        break;
                
                    case ReplaySpeed.Double:
                        result = globalSavingTimeInterval * 0.5f;
                        break;
                
                    case ReplaySpeed.FourTimes:
                        result = globalSavingTimeInterval * 0.25f;
                        break;
                }
            
                return result;
            }
        }
    }
}