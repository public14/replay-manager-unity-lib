﻿using Replay.Common.Enum;
using UnityEngine;

namespace Replay.Common.Static
{
    /// <summary> 리플레이 전역 상수들 </summary>
    public static class GlobalConstants
    {

        // =============================================================================
        // Event Key 
        /// <summary> 시뮬레이션 시작 이벤트 키 </summary>
        public const string EVENT_KEY_SIMULATION_START = "SIMULATION_START";

        /// <summary> 시뮬레이션 종료 이벤트 키 </summary>
        public const string EVENT_KEY_SIMULATION_END = "SIMULATION_END";

        /// <summary> 리플레이 데이터 로드 이벤트 키 </summary>
        public const string EVENT_KEY_REPLAY_DATA_LOAD = "REPLAY_DATA_LOAD";

        /// <summary> 리플레이 시작 키 </summary>
        public const string EVENT_KEY_REPLAY_START = "REPLAY_START";

        /// <summary> 리플레이 종료 키 </summary>
        public const string EVENT_KEY_REPLAY_END = "REPLAY_END";

        /// <summary> 게임오브젝트 생성 이벤트 키 </summary>
        public const string EVENT_KEY_ENABLE_GAMEOBJECT = "ENABLE_GAMEOBJECT";

        /// <summary> 게임오브젝트 파괴 이벤트 키 </summary>
        public const string EVENT_KEY_DISABLE_GAMEOBJECT = "DISABLE_GAMEOBJECT";

        // =============================================================================
        // Save Key 
        /// <summary> 활성화된 인스턴스 맥스 수량 저장 키 </summary>
        public const string SAVE_KEY_INSTANCE_MAX_COUNT = "INSTANCE_MAX_COUNT";

        /// <summary> 프리팹키 리스트 저장 키 </summary>
        public const string SAVE_KEY_PREFAB_KEY_LIST = "PREFAB_KEY_LIST";

        /// <summary> 인스턴스 스냅샷 저장 키 </summary>
        public const string SAVE_KEY_ALL_INSTANCES_SNAPSHOT = "ALL_INSTANCES_SNAPSHOT";

        /// <summary> 활성화된 인스턴스 스냅샷 저장 키 </summary>
        public const string SAVE_KEY_ENABLED_INSTANCES_SNAPSHOT = "ENABLED_INSTANCES_SNAPSHOT";

        /// <summary> 비활성화된 인스턴스 스냅샷 저장 키 </summary>
        public const string SAVE_KEY_DISABLED_INSTANCES_SNAPSHOT = "DISABLED_INSTANCES_SNAPSHOT";

        /// <summary> 게임오브젝트 전체데이터 스냅샷 저장 키 </summary>
        public const string SAVE_KEY_FULL_GAMEOBJECT_DATA_SNAPSHOT = "FULL_GAMEOBJECT_DATA_SNAPSHOT";

        /// <summary> 게임오브젝트 커스텀데이터 스냅샷 저장 키 </summary>
        public const string SAVE_KEY_CUSTOM_GAMEOBJECT_DATA_SNAPSHOT = "CUSTOM_GAMEOBJECT_DATA_SNAPSHOT";

        /// <summary> 게임오브젝트 전체데이터 스냅샷 전체 저장 키 </summary>
        public const string SAVE_KEY_FULL_GAMEOBJECTS_DATA_SNAPSHOTS_ALL = "FULL_GAMEOBJECTS_DATA_SNAPSHOTS_ALL";

        /// <summary> 게임오브젝트 커스텀데이터 스냅샷 전체 저장 키 </summary>
        public const string SAVE_KEY_CUSTOM_GAMEOBJECTS_DATA_SNAPSHOT_ALL = "CUSTOM_GAMEOBJECTS_DATA_SNAPSHOT_ALL";

        /// <summary> PlayId 저장 키 </summary>
        public const string SAVE_KEY_PLAY_ID = "PLAY_ID";

        /// <summary> 시작시간 저장 키 </summary>
        public const string SAVE_KEY_START_DATE_TIME = "START_DATE_TIME";

        /// <summary> 종료시간 저장 키 </summary>
        public const string SAVE_KEY_END_DATE_TIME = "END_DATE_TIME";

        /// <summary> 시작시간(유닉스) 저장 키 </summary>
        public const string SAVE_KEY_START_UNIX_TIME = "START_UNIX_TIME";

        /// <summary> 종료시간(유닉스) 저장 키 </summary>
        public const string SAVE_KEY_END_UNIX_TIME = "END_UNIX_TIME";

        /// <summary> 종료시퀀스 저장 키 </summary>
        public const string SAVE_KEY_END_SEQUENCE = "END_SEQUENCE";

        /// <summary> 저장 시간 간격 저장 키 </summary>
        public const string SAVE_KEY_SAVING_TIME_INTERVAL = "SAVING_TIME_INTERVAL";
        
        /// <summary> 저장 데이터 단위 저장 키 </summary>
        public const string SAVE_KEY_SAVE_DATA_UNIT = "SAVE_DATA_UNIT";

        // =============================================================================
        // Directory
        /// <summary> 최상위 디렉토리명 </summary>
        public const string DIRECTORY_NAME_ON_TOP = "ReplayDataStorage";
        
        /// <summary> 차상위 디렉토리 접두사 Ex. Simulation_12345678  </summary>
        public const string DIRECTORY_NAME_PREFIX_ON_SECOND = "Simulation";

        /// <summary> 현재 시뮬레이션의 공통정보를 저장할 파일명 </summary>
        public const string FILE_NAME_GLOBAL_INFO = "GlobalInfomation.ecs";

        /// <summary> 현재 시뮬레이션의 공통정보를 저장할 파일명 </summary>
        public const string FILE_NAME_PREFABKEY_GAMEOBJECTNAME_PAIR = "PrefabKeyGameObjectNamePair.ecs";

        // =============================================================================
        // Coroutine
        /// <summary> 중앙 관리자가 스냅샷을 찍는 코루틴 태그값 </summary>
        public const string COROUTINE_TAG_CENTRAL_SNAPSHOT = "CENTRAL_SNAPSHOT";

        public const string COROUTINE_TAG_CENTRAL_SIMULATION_END = "CENTRAL_SIMULATION_END";
        
        /// <summary> ES3 세팅 </summary>
        // public static readonly ES3Settings GlobalES3Settings = new ES3Settings(ES3.CompressionType.None, ES3.EncryptionType.None, ES3.Format.JSON, ES3.Location.File);
        public static readonly ES3Settings GlobalES3Settings = new ES3Settings(ES3.CompressionType.Gzip, ES3.EncryptionType.None, ES3.Format.JSON, ES3.Location.File);
        
        // =============================================================================
        // Object Name
        /// <summary> 리플레이 오브젝트에 붙을 접두수식어 </summary>
        public const string REPLAY_GAMEOBJECT_SUFFIX = "_REPLAY";

        /// <summary> 저장할 데이터 단위 </summary>
        public static readonly SaveDataUnit SAVE_DATA_UNIT = SaveDataUnit.CustomGameObjectData;

        public static readonly string ROOT_DIRECTORY = Application.dataPath.Replace("Assets", "");
        // public static readonly string ROOT_DIRECTORY = Application.persistentDataPath;

    }
}