﻿namespace Replay.Common.Static
{
    /// <summary> 리플레이 전역 로그 메시지 </summary>
    public static class GlobalLogMessages
    {
        /// <summary> 잘못된 Input을 보냈을 때 메시지 </summary>
        public const string MESSAGE_WRONG_INPUT = "Invalid value has been delivered.";
        
        /// <summary> Null Input을 보냈을 때 메시지 </summary>
        public const string MESSAGE_NULL_INPUT = "Null value has been delivered.";

        public const string MESSAGE_PREFAB_KEY_IS_NULL = "You have to define 'PrefabKey' on your inspector. 프리팹키가 설정되어 있지 않습니다.";
    }
}