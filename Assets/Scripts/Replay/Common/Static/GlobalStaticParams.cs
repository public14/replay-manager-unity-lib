﻿namespace Replay.Common.Static
{
    /// <summary> 리플레이 전역 정적 변수들 </summary>
    public static class GlobalStaticParams
    {
        /// <summary> 전역 시퀀스 </summary>
        public static int GlobalSequence { get; set; } = 1;
        
        /// <summary> 전역 디렉토리 </summary>
        public static string ReplayDirectory { get; set; }
    }
}