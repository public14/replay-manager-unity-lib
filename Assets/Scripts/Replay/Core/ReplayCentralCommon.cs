﻿using com.ootii.Messages;
using Replay.Data;
using Sisus.Init;
using UnityEngine;

namespace Replay.Core
{
    public abstract class ReplayCentralCommon : MonoBehaviour<string, long, float> //playId, dateTime, savingTimeInterval
    {
        protected ArchivePrefab ArchivePrefab = new ArchivePrefab(); // 프리팹 보관소 클래스
        protected ArchiveMaxCount ArchiveMaxCount = new ArchiveMaxCount(); // 프리팹별 최대수량 보관소 클래스
        protected TempAllInstances TempAllInstances = new TempAllInstances(); // 활성화되어 있는 전체 인스턴스 임시 보관소 클래스 
        protected TempEnabledInstances TempEnabledInstances = new TempEnabledInstances(); // 활성화시킨 인스턴스 임시 보관소 클래스 
        protected TempDisabledInstances TempDisabledInstances = new TempDisabledInstances(); // 비활성화시킨 인스턴스 임시 보관소 클래스
        protected ArchiveAllInstancesSnapshot ArchiveAllInstancesSnapshot = new ArchiveAllInstancesSnapshot(); // 활성화되어 있는 인스턴스의 스냅샷 보관소 클래스
        protected ArchiveEnabledInstancesSnapshot ArchiveEnabledInstancesSnapshot = new ArchiveEnabledInstancesSnapshot(); // 활성화시킨 인스턴스의 스냅샷 보관소 클래스
        protected ArchiveDisabledInstancesSnapshot ArchiveDisabledInstancesSnapshot = new ArchiveDisabledInstancesSnapshot(); // 비활성화시킨 인스턴스의 스냅샷 보관소 클래스
        public ArchiveFullGameObjectDataSnapshots ArchiveFullGameObjectDataSnapshots = new ArchiveFullGameObjectDataSnapshots(); 
        public ArchiveCustomGameObjectDataSnapshots ArchiveCustomGameObjectDataSnapshots = new ArchiveCustomGameObjectDataSnapshots();
        protected ArchiveSimulationInfo ArchiveSimulationInfo = new ArchiveSimulationInfo();
        
        /// <summary> 시작 </summary>
        public virtual void OnStart(){}
        
        /// <summary> 종료 </summary>
        public virtual void OnEnd(){}
        
        public virtual void OnEventEnableGameObject(IMessage incomingMessage) {}

        public virtual void OnEventDisableGameObject(IMessage incomingMessage) {}
    }
}