﻿using System.IO;
using Replay.Common.Enum;
using Replay.Common.Static;
using UnityEngine;
using Sisus.Init;

namespace Replay.Core
{
    public static class ReplayCentralFactory
    {
        public static ReplayCentralCommon Create(ReplayMode replayMode, string playId, float savingTimeInterval = float.MaxValue, long dateTime = long.MaxValue)
        {
            ReplayCentralCommon replayCentralCommon;
            switch (replayMode)
            {
                case ReplayMode.Save:
                {
                    replayCentralCommon = ReplayManager.Instance().gameObject.AddComponent<ReplayCentralSaver, string, long, float>(playId, dateTime, savingTimeInterval);
                    replayCentralCommon.OnStart();
                    break;
                }
                case ReplayMode.Load:
                {
                    replayCentralCommon = ReplayManager.Instance().gameObject.AddComponent<ReplayCentralLoader, string, long, float>(playId, dateTime, savingTimeInterval);
                    break;
                }
                default:
                    replayCentralCommon = null;
                    Debug.LogError(GlobalLogMessages.MESSAGE_WRONG_INPUT);
                    break;
            }
            return replayCentralCommon;
        }

    }
}