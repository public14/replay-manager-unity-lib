﻿using System;
using System.Collections.Generic;
using System.IO;
using Init.Demo;
using MEC;
using PathologicalGames;
using Replay.Common.Enum;
using Replay.Common.Static;
using Sisus.Init;
using UnityEngine;

namespace Replay.Core
{
    public class ReplayCentralLoader : ReplayCentralCommon
    {
        protected override void Init(string playId, long startUnixTime, float savingTimeInterval)
        {
            GlobalStaticParams.ReplayDirectory = this.FindLoadDirectory(playId);
            this.OnLoadReplayFile();
            this.InitializeObjectPool();
        }

        /// <summary>
        /// 시작
        /// </summary>
        public override void OnStart()
        {
            var cor = ReplayCoroutineFactory.CreateCoroutine(ReplayMode.Load, true);
            cor.InvokeAction += SpawnAndDespawn;
            Timing.KillCoroutines(GlobalConstants.COROUTINE_TAG_CENTRAL_SNAPSHOT);
            Timing.RunCoroutine(cor.OnExecute(), GlobalConstants.COROUTINE_TAG_CENTRAL_SNAPSHOT);
        }
        
        /// <summary>
        /// 종료
        /// </summary>
        public override void OnEnd()
        {
            Timing.KillCoroutines(GlobalConstants.COROUTINE_TAG_CENTRAL_SNAPSHOT);
        }

        /// <summary>
        /// 리플레이 파일 불러오기
        /// </summary>
        private void OnLoadReplayFile()
        {
            string rootPath = GlobalStaticParams.ReplayDirectory;
            string filePath = Path.Combine(rootPath, GlobalConstants.FILE_NAME_GLOBAL_INFO);
            this.ArchiveSimulationInfo.FileLoad(filePath);
            ReplayManager.Instance().savingTimeInterval = this.ArchiveSimulationInfo.SavingTimeInterval;
            this.ArchivePrefab.FileLoad(filePath);
            this.ArchiveMaxCount.FileLoad(filePath);
            this.ArchiveAllInstancesSnapshot.FileLoad(filePath);
            this.ArchiveEnabledInstancesSnapshot.FileLoad(filePath);
            this.ArchiveDisabledInstancesSnapshot.FileLoad(filePath);

            if (GlobalConstants.SAVE_DATA_UNIT == SaveDataUnit.FullGameObjectData)
            {
                this.ArchiveFullGameObjectDataSnapshots.FileLoad(rootPath);
            }
            else if (GlobalConstants.SAVE_DATA_UNIT == SaveDataUnit.CustomGameObjectData)
            {
                this.ArchiveCustomGameObjectDataSnapshots.FileLoad(rootPath);
            }
        }
        
        /// <summary>
        /// 오브젝트풀 초기화
        /// </summary>
        private void InitializeObjectPool()
        {
            foreach (var prefabKey in ArchivePrefab.ListPrefabKey)
            {
                string poolName = prefabKey;

                if (PoolManager.Pools.ContainsKey(poolName))
                {
                    Debug.LogError($"현재 오브젝트풀이 생성된 프리팹인데, 중복 생성을 시도했습니다. prefabKey: {prefabKey}");
                    continue;
                }

                PoolManager.Pools.Create(poolName);
                PoolManager.Pools[poolName].matchPoolLayer = true;

                var prefabPool = new PrefabPool(ReplayManager.Instance().dicPrefab[prefabKey].transform)
                    {
                        preloadAmount = ArchiveMaxCount.DicMaxCount[prefabKey],
                        _logMessages = true
                    };

                PoolManager.Pools[poolName].CreatePrefabPool(prefabPool);
                PoolManager.Pools[poolName].DespawnAll();
                
                // http://docs.poolmanager.path-o-logical.com/code-reference/prefabpool
            }
        }

        private void SpawnAndDespawn(int sequence)
        {
            if (this.ArchiveEnabledInstancesSnapshot.DicEnabledInstancesSnapshot.TryGetValue(sequence, out var dicSpawn))
            {
                foreach (var variable in dicSpawn)
                {
                    if (string.IsNullOrEmpty(variable.Value)) continue;
                    Debug.Log($"SpawnPrefabs sequence: {sequence}, objNames : {variable.Value}");
                    char[] delimiterChars = {';'};
                    string[] objNamesForEnable = variable.Value.Split(delimiterChars, StringSplitOptions.RemoveEmptyEntries);
                    
                    foreach (var objName in objNamesForEnable)
                    {
                        if (string.IsNullOrEmpty(objName)) continue;

                        var instance = PoolManager.Pools[variable.Key].Spawn(ReplayManager.Instance().dicPrefab[variable.Key]);
                        instance.name = objName + GlobalConstants.REPLAY_GAMEOBJECT_SUFFIX;
                        // Debug.Log($"Spawn Prefab : {objName}");
                        
                        // var param = Path.Combine(rootDirectory, variable.Key);
                        IInputManager inputManager = new InputManager();
                        var obj = instance.gameObject.AddComponent<ReplayDataInjector, IInputManager>(inputManager);
                        obj.OnStartInjection();
                    }
                }
            }
            
            if (this.ArchiveDisabledInstancesSnapshot.DicDisabledInstancesSnapshot.TryGetValue(sequence, out var dicDespawn))
            {
                foreach (var variable in dicDespawn)
                {
                    if(string.IsNullOrEmpty(variable.Value)) continue;
                    Debug.Log($"DespawnPrefabs sequence: {sequence}, objNames : {variable.Value}");
                    SpawnPool pool = PoolManager.Pools[variable.Key];
                    var spawnedCopy = new List<Transform>(pool);
                
                    char[] delimiterChars = { ';' };
                    string[] objNamesForDisable = variable.Value.Split(delimiterChars, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var objName in objNamesForDisable)
                    {
                        if(string.IsNullOrEmpty(objName)) continue;
                        var instance = spawnedCopy.Find(x => x.gameObject.name == objName.Trim() + GlobalConstants.REPLAY_GAMEOBJECT_SUFFIX); 
                        if (instance != null && instance.gameObject.TryGetComponent<ReplayDataInjector>(out var component))
                        {
                            component.OnEndInjection();
                            Destroy(component);
                        }
                        pool.Despawn(instance);
                        // Debug.Log($"Despawn Prefab : {objName}");
                    }
                }
            }
        }

        /// <summary>
        /// 불러올 디렉토리 찾기
        /// </summary>
        /// <param name="playId"></param>
        /// <returns></returns>
        private string FindLoadDirectory(string playId)
        {
            var rootPath = Path.Combine(GlobalConstants.ROOT_DIRECTORY, GlobalConstants.DIRECTORY_NAME_ON_TOP);
            var dirs = Directory.GetDirectories(rootPath);

            var path = string.Empty;
            foreach (var dir in dirs)
            {
                // 해당 PlayId로 된 리플레이 디렉토리를 찾는다. 
                if (dir.Contains(playId))
                {
                    path = dir;
                }
            }
            return path;
        }
    }
}