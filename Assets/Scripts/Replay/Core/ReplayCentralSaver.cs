﻿using System;
using System.Collections.Generic;
using System.IO;
using com.ootii.Messages;
using MEC;
using Replay.Common.Enum;
using Replay.Common.Messages;
using Replay.Common.Static;
using Debug = UnityEngine.Debug;

namespace Replay.Core
{
    public class ReplayCentralSaver : ReplayCentralCommon
    {
        /// <summary>
        /// 초기화
        /// </summary>
        /// <param name="playId"></param>
        /// <param name="startUnixTime"></param>
        /// <param name="saveTimeInterval"></param>
        protected override void Init(string playId, long startUnixTime, float saveTimeInterval)
        {
            this.ArchiveSimulationInfo.PlayId = playId;
            this.ArchiveSimulationInfo.StartDateTime = DateTime.Now;
            this.ArchiveSimulationInfo.StartUnixTime = startUnixTime;
            this.ArchiveSimulationInfo.SaveDataUnit = GlobalConstants.SAVE_DATA_UNIT;
            this.ArchiveSimulationInfo.SavingTimeInterval = saveTimeInterval;
            GlobalStaticParams.ReplayDirectory = this.CreateSaveDirectory(playId, startUnixTime);
        }

        public override void OnStart()
        {
            var cor = ReplayCoroutineFactory.CreateCoroutine(ReplayMode.Save, true);
            cor.InvokeAction += this.OnUpdateSnapshotBySequence;
            Timing.RunCoroutine(cor.OnExecute(), GlobalConstants.COROUTINE_TAG_CENTRAL_SNAPSHOT);
        }

        public override void OnEnd()
        {
            Timing.KillCoroutines(GlobalConstants.COROUTINE_TAG_CENTRAL_SNAPSHOT); // 코루틴 종료
            string rootPath = GlobalStaticParams.ReplayDirectory;
            string filePath = Path.Combine(rootPath, GlobalConstants.FILE_NAME_GLOBAL_INFO);
            this.ArchiveSimulationInfo.EndSequence = GlobalStaticParams.GlobalSequence;
            this.ArchiveSimulationInfo.EndDateTime = DateTime.Now;
            this.ArchiveSimulationInfo.EndUnixTime = System.DateTimeOffset.Now.ToUnixTimeMilliseconds();
            this.ArchiveSimulationInfo.FileSave(filePath);
            this.ArchiveMaxCount.FileSave(filePath);
            this.ArchivePrefab.FileSave(filePath);
            this.ArchiveAllInstancesSnapshot.FileSave(filePath);
            this.ArchiveEnabledInstancesSnapshot.FileSave(filePath);
            this.ArchiveDisabledInstancesSnapshot.FileSave(filePath);
            this.ArchiveFullGameObjectDataSnapshots.FileSave(rootPath);
            this.ArchiveCustomGameObjectDataSnapshots.FileSave(rootPath);
        }

        private void OnUpdateSnapshotBySequence(int sequence)
        {
            this.ArchiveAllInstancesSnapshot.TryAdd(sequence, new Dictionary<string, string>(this.TempAllInstances.DicAllInstancesTemporary));
            this.ArchiveEnabledInstancesSnapshot.TryAdd(sequence, new Dictionary<string, string>(this.TempEnabledInstances.DicEnabledInstancesTemporary));
            this.TempEnabledInstances.DicEnabledInstancesTemporary.Clear();
            // this.TempEnabledInstances.Clear();
            this.ArchiveDisabledInstancesSnapshot.TryAdd(sequence, new Dictionary<string, string>(this.TempDisabledInstances.DicDisabledInstancesTemporary));
            // this.TempDisabledInstances.Clear();
            this.TempDisabledInstances.DicDisabledInstancesTemporary.Clear();
        }

        /// <summary>
        /// 프리팹 생성 시점을 기록
        /// </summary>
        /// <param name="incomingMessage"></param>
        public override void OnEventEnableGameObject(IMessage incomingMessage)
        {
            var eventData = (CustomEventDataObject) incomingMessage.Data;
            Debug.Log($"프리팹 인스턴스 생성 기록 완료. Sequence: {GlobalStaticParams.GlobalSequence}, PrefabKey: {eventData.PrefabKey}, GameObjectName: {eventData.GameObjectName}");
            this.ArchivePrefab.TryAdd(eventData.PrefabKey);
            this.TempAllInstances.TryAdd(eventData.PrefabKey, eventData.GameObjectName);
            this.TempEnabledInstances.TryAdd(eventData.PrefabKey, eventData.GameObjectName);
            this.ArchiveMaxCount.TryAdd(eventData.PrefabKey, this.TempAllInstances.GetMaxInstanceCount(eventData.PrefabKey));
            this.ArchiveFullGameObjectDataSnapshots.TryAdd(eventData.PrefabKey, eventData.GameObjectName);
            this.ArchiveCustomGameObjectDataSnapshots.TryAdd(eventData.PrefabKey, eventData.GameObjectName);
        }

        /// <summary>
        /// 프리팹 파괴 시점을 기록 
        /// </summary>
        /// <param name="incomingMessage"></param>
        public override void OnEventDisableGameObject(IMessage incomingMessage)
        {
            var eventData = (CustomEventDataObject) incomingMessage.Data;
            Debug.Log($"프리팹 인스턴스 파괴 기록 완료 : {eventData}");
            this.TempAllInstances.TryDelete(eventData.PrefabKey, eventData.GameObjectName);
            this.TempDisabledInstances.TryAdd(eventData.PrefabKey, eventData.GameObjectName);
        }
        
        /// <summary>
        /// 저장 디렉토리 생성
        /// </summary>
        /// <param name="playId"></param>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        private string CreateSaveDirectory(string playId, long dateTime)
        {
            var path = Path.Combine(GlobalConstants.ROOT_DIRECTORY, GlobalConstants.DIRECTORY_NAME_ON_TOP,
                $"{GlobalConstants.DIRECTORY_NAME_PREFIX_ON_SECOND}_{dateTime}_{playId}");
            Directory.CreateDirectory(path);
            return path;
        }

    }
}