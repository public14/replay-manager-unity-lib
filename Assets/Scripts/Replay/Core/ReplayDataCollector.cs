﻿using System;
using System.Collections.Generic;
using com.ootii.Messages;
using Init.Demo;
using MEC;
using Replay.Common.Enum;
using Replay.Common.Messages;
using Replay.Common.Static;
using Replay.Data;
using Sisus.Init;
using UnityEngine;

namespace Replay.Core
{
    public class ReplayDataCollector : MonoBehaviour<IInputManager>
    {
        private TempFullGameObjectDataSnapshot _tempFullGameObjectDataSnapshot = new TempFullGameObjectDataSnapshot();
        private TempCustomGameObjectDataSnapshot _tempCustomGameObjectDataSnapshot = new TempCustomGameObjectDataSnapshot();
        
        protected override void Init(IInputManager argument){}

        /// <summary> 현재 컴포넌트가 활성화되면서 실행되는 초기화 </summary>
        protected override void OnAwake()
        {
            Timing.RunCoroutine(CorSendMessageWhenInstantiate());
        }

        /// <summary> 오브젝트 활성화 </summary>
        void OnEnable()
        {
            Timing.RunCoroutine(CorSendMessageWhenInstantiate());
        }

        /// <summary> 오브젝트 비활성화 </summary>
        void OnDisable()
        {
            this.OnEndCollecting(); // 데이터 수집 종료
            this.OnSendMessageWhenDestroy(); // 비활성화되었음을 중앙에 알린다.
        }
        
        /// <summary> 데이터 수집 시작 </summary>
        private void OnStartCollecting()
        {
            var cor = ReplayCoroutineFactory.CreateCoroutine(ReplayMode.Save, false);
            cor.InvokeAction += this.OnCollectSequencedData;
            Timing.RunCoroutine(cor.OnExecute(), gameObject.GetInstanceID().ToString());
        }

        /// <summary> 데이터 수집 종료 </summary>
        public void OnEndCollecting()
        {
            // 데이터 수집 종료는 (1) 오브젝트가 비활성화되거나, (2) ReplayManager에서 시뮬레이션을 종료시켰을 때 실행된다. 
            Timing.KillCoroutines(gameObject.GetInstanceID().ToString()); // 코루틴 종료
            this.SendAllDataToCentral();
        }

        /// <summary>
        /// 수집된 데이터 전체를 중앙으로 전송한다.
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        private void SendAllDataToCentral()
        {
            if (ReplayManager.Instance().replayCentralCommon == null) return;
            var obj = this.gameObject;
            switch (GlobalConstants.SAVE_DATA_UNIT)
            {
                case SaveDataUnit.FullGameObjectData:
                    ReplayManager.Instance().replayCentralCommon.ArchiveFullGameObjectDataSnapshots.TryAdd(
                        obj.GetComponent<ReplayPrefabKeyRegister>().prefabKey,
                        obj.name,
                        this._tempFullGameObjectDataSnapshot.DicFullGameObjectDataSnapshot
                    );
                    break;
                case SaveDataUnit.CustomGameObjectData:
                    ReplayManager.Instance().replayCentralCommon.ArchiveCustomGameObjectDataSnapshots.TryAdd(
                        obj.GetComponent<ReplayPrefabKeyRegister>().prefabKey,
                        obj.name,
                        this._tempCustomGameObjectDataSnapshot.DicCustomGameObjectDataSnapshot
                    );
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            Debug.Log("ReplayCentralManager.Instance().gatheredCount++");
            ReplayManager.Instance().localSaverEndCount++;
        }

        /// <summary>
        /// 시퀀스에 따라 데이터를 수집한다.
        /// </summary>
        /// <param name="sequence"></param>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        private void OnCollectSequencedData(int sequence)
        {
            switch (GlobalConstants.SAVE_DATA_UNIT)
            {
                case SaveDataUnit.FullGameObjectData:
                    this._tempFullGameObjectDataSnapshot.TryAdd(sequence, this.gameObject);
                    break;

                case SaveDataUnit.CustomGameObjectData:  
                    var t = this.transform;
                    var go = new GameObject();
                    go.transform.parent = ReplayManager.Instance().transform;
                    go.SetActive(false);
                    go.transform.SetPositionAndRotation(t.position, t.rotation);
                    this._tempCustomGameObjectDataSnapshot.TryAdd(sequence, go);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        
        /// <summary>
        /// 활성화 이벤트 전송 전 0.1f 딜레이 구현한 코루틴
        /// </summary>
        /// <returns></returns>
        private IEnumerator<float> CorSendMessageWhenInstantiate()
        {
            yield return Timing.WaitForSeconds(0.1f); // 잠시 대기를 시키지 않으면 이벤트 Listen을 못함..
            this.OnSendMessageWhenInstantiate();
            this.OnStartCollecting();
        }

        /// <summary>
        /// 프리팹 활성화(or생성) 시 이벤트 메시지 전송 to ReplayManager
        /// </summary>
        private void OnSendMessageWhenInstantiate()
        {
            var customEventDataObject = new CustomEventDataObject(this.gameObject.GetComponent<ReplayPrefabKeyRegister>().prefabKey, this.gameObject.name);
            MessageDispatcher.SendMessageData(GlobalConstants.EVENT_KEY_ENABLE_GAMEOBJECT, customEventDataObject); 
        }

        /// <summary>
        /// 프리팹 비활성화(or파괴) 시 이벤트 메시지 전송
        /// </summary>
        private void OnSendMessageWhenDestroy()
        {
            var customEventDataObject = new CustomEventDataObject(this.gameObject.GetComponent<ReplayPrefabKeyRegister>().prefabKey, this.gameObject.name);
            MessageDispatcher.SendMessageData(GlobalConstants.EVENT_KEY_DISABLE_GAMEOBJECT, customEventDataObject); 
        }

    }
}