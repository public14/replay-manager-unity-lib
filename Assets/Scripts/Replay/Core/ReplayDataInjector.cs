﻿using System.Collections.Generic;
using Init.Demo;
using MEC;
using Replay.Common.Enum;
using Replay.Common.Static;
using Replay.Data;
using Sisus.Init;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace Replay.Core
{
    public class ReplayDataInjector : MonoBehaviour<IInputManager>
    {
        private TempFullGameObjectDataSnapshot _tempFullGameObjectDataSnapshot = new TempFullGameObjectDataSnapshot();
        private TempCustomGameObjectDataSnapshot _tempCustomGameObjectDataSnapshot = new TempCustomGameObjectDataSnapshot();

        protected override void Init(IInputManager argument) { }

        protected override void OnAwake()
        {
            Debug.Log($"OnAwake ReplayLocalLoader is Initialized. obj: {this.name}");
            TryDisableComponent<Rigidbody>();
            this.OnLoadReplayDataFromCentral();
        }

        /// <summary>
        /// 데이터 주입 시작
        /// </summary>
        public void OnStartInjection()
        {
            var cor = ReplayCoroutineFactory.CreateCoroutine(ReplayMode.Load, false);
            cor.InvokeAction = this.OnInjectSequencedData;
            Timing.RunCoroutine(cor.OnExecute(), gameObject.GetInstanceID().ToString());
        }

        /// <summary>
        /// 데이터 주입 종료
        /// </summary>
        public void OnEndInjection()
        {
            Timing.KillCoroutines(gameObject.GetInstanceID().ToString());
        }
        
        /// <summary>
        /// 리플레이 데이터 불러오기
        /// </summary>
        private void OnLoadReplayDataFromCentral()
        {
            string gameObjectName = this.gameObject.name.Replace(GlobalConstants.REPLAY_GAMEOBJECT_SUFFIX, string.Empty);

            // 인스턴스 생성 시점에 파일을 읽어들이는 것이 아니라,
            // 리플레이 최초에 메모리에 다 로드시켜놓고, 인스턴스 생성 시점에는 메모리에서 불러오는 방식으로 수정
            if (GlobalConstants.SAVE_DATA_UNIT == SaveDataUnit.FullGameObjectData)
            {
                this._tempFullGameObjectDataSnapshot.DicFullGameObjectDataSnapshot = this.GetSequencedGameObjectData(this.gameObject.GetComponent<ReplayPrefabKeyRegister>().prefabKey, gameObjectName);
                Debug.Log($"{this.gameObject.name} DicObjectDataBySequence Loaded. Data Count: {this._tempFullGameObjectDataSnapshot.DicFullGameObjectDataSnapshot.Count}");
            }
            else if (GlobalConstants.SAVE_DATA_UNIT == SaveDataUnit.CustomGameObjectData)
            {
                this._tempCustomGameObjectDataSnapshot.DicCustomGameObjectDataSnapshot = this.GetSequencedCustomData(this.gameObject.GetComponent<ReplayPrefabKeyRegister>().prefabKey, gameObjectName);
                Debug.Log($"{this.gameObject.name} DicCustomDataBySequence Loaded. Data Count: {this._tempCustomGameObjectDataSnapshot.DicCustomGameObjectDataSnapshot.Count}");
            }
        }

        private Dictionary<int, GameObject> GetSequencedGameObjectData(string prefabKey, string gameObjectName)
        {
            if(ReplayManager.Instance().replayCentralCommon.ArchiveFullGameObjectDataSnapshots.DicFullGameObjectDataSnapshots.TryGetValue(prefabKey, out var dicSequencedDataOfPrefab))
            {
                if (dicSequencedDataOfPrefab.TryGetValue(gameObjectName, out var dicSequencedDataOfPrefabInstance))
                {
                    return dicSequencedDataOfPrefabInstance;
                }
                Debug.LogError($"dicSequencedDataOfPrefabInstance를 찾지 못했습니다. prefabKey: {prefabKey}, gameObjectName: {gameObjectName}");
                return null;
            }
            Debug.LogError($"dicSequencedDataOfPrefab를 찾지 못했습니다. prefabKey: {prefabKey}");
            return null;
        }

        private Dictionary<int, GameObject> GetSequencedCustomData(string prefabKey, string gameObjectName)
        {
            if(ReplayManager.Instance().replayCentralCommon.ArchiveCustomGameObjectDataSnapshots.DicCustomGameObjectDataSnapshot.TryGetValue(prefabKey, out var dicSequencedCustomDataOfPrefab))
            {
                if (dicSequencedCustomDataOfPrefab.TryGetValue(gameObjectName, out var dicSequencedCustomDataOfPrefabInstance))
                {
                    return dicSequencedCustomDataOfPrefabInstance;
                }
                Debug.LogError($"dicSequencedDataOfPrefabInstance를 찾지 못했습니다. prefabKey: {prefabKey}, gameObjectName: {gameObjectName}");
                return null;
            }
            Debug.LogError($"dicSequencedDataOfPrefab를 찾지 못했습니다. prefabKey: {prefabKey}");
            return null;
        }
        
        /// <summary>
        /// 데이터 주입을 서열에 따라 실행한다.
        /// </summary>
        /// <param name="sequence"></param>
        private void OnInjectSequencedData(int sequence)
        {
            switch (GlobalConstants.SAVE_DATA_UNIT)
            {
                case SaveDataUnit.FullGameObjectData:
                    if (this._tempFullGameObjectDataSnapshot.DicFullGameObjectDataSnapshot.TryGetValue(sequence, out var fullGameObj))
                    {
                        var t = fullGameObj.transform;
                        transform.SetPositionAndRotation(t.position, t.rotation);
                    }
                    else
                    {
                        Debug.Log($"DicObjectDataBySequence 값을 찾아올 수 없습니다. gameObject: {this.gameObject.name}, sequence: {sequence} ");
                    }
                    break;
                
                case SaveDataUnit.CustomGameObjectData:
                    if (this._tempCustomGameObjectDataSnapshot.DicCustomGameObjectDataSnapshot.TryGetValue(sequence, out var customGameObj))
                    {
                        var t = customGameObj.transform;
                        transform.SetPositionAndRotation(t.position, t.rotation);
                    }
                    break;
            }
        }
        
        private bool IsDeleteType<T>()
        {
            if(typeof(T) == typeof(BoxCollider)) return true;
            else if(typeof(T) == typeof(SphereCollider)) return true;
            else if(typeof(T) == typeof(Rigidbody)) return true;
            else if(typeof(T) == typeof(SendColor)) return true;
            else if(typeof(T) == typeof(ListenForColor)) return true;
            return false;
        }

        /// <summary>
        /// 컴포넌트 제거 시도
        /// </summary>
        /// <typeparam name="T"></typeparam>
        private void TryDestroyComponent<T>()
        {
            if (this.gameObject.TryGetComponent<T>(out var thisType))
            {
                if (IsDeleteType<T>())
                {
                    Destroy(thisType as BoxCollider);
                    Debug.Log($"{this.gameObject.name} 컴포넌트 제거 완료 {typeof(T)}");
                    return;
                }
                Debug.Log($"{this.gameObject.name} 컴포넌트 제거 실패 - 정의된 제거 타입이 아닙니다.");
                return;
            }
            Debug.Log($"{this.gameObject.name} 컴포넌트 제거 실패 - {typeof(T)} 때문");
            // 소스코드 참고: https://dragontory.tistory.com/79
        }
        
        /// <summary>
        /// 컴포넌트 제거 시도
        /// </summary>
        /// <typeparam name="T"></typeparam>
        private void TryDisableComponent<T>()
        {
            if (this.gameObject.TryGetComponent<T>(out var thisType))
            {
                if (typeof(T) == typeof(BoxCollider))
                {
                    var component = FindObjectOfType<BoxCollider>();
                    if(component != null) component.enabled = false;
                    return;
                }
                else if (typeof(T) == typeof(SphereCollider))
                {
                    var component = FindObjectOfType<SphereCollider>();
                    if(component != null) component.enabled = false;
                    return;
                }
                else if (typeof(T) == typeof(Rigidbody))
                {
                    var component = FindObjectOfType<Rigidbody>();
                    if (component != null) DestroyImmediate(component);
                    return;
                }
                else if (typeof(T) == typeof(SendColor))
                {
                    var component = FindObjectOfType<SendColor>();
                    if(component != null) component.enabled = false;
                    return;
                }
                else if (typeof(T) == typeof(ListenForColor))
                {
                    var component = FindObjectOfType<ListenForColor>();
                    if(component != null) component.enabled = false;
                    return;
                }
                else
                {
                    Debug.Log($"{this.gameObject.name} 컴포넌트 제거 실패 - 정의된 제거 타입이 아닙니다.");
                    return;
                }
            }
            Debug.Log($"{this.gameObject.name} 컴포넌트 제거 실패 - {typeof(T)} 때문");
            // 소스코드 참고: https://dragontory.tistory.com/79
        }
        
        /// <summary>
        /// 컴포넌트 복사 시도
        /// </summary>
        /// <param name="savedObj"></param>
        /// <typeparam name="T"></typeparam>
        private void TryCopyComponent<T>(GameObject savedObj)
        {
            if (this.gameObject.TryGetComponent<T>(out var thisType))
            {
                if (savedObj.TryGetComponent<T>(out var savedType))
                {
                    Debug.Log($"{this.gameObject.name} 컴포넌트 복사 완료 {typeof(T)}");
                    thisType = savedType;
                    return;
                }
                Debug.Log($"{this.gameObject.name} 컴포넌트 복사 실패 - savedType 때문");
                return;
            }
            Debug.Log($"{this.gameObject.name} 컴포넌트 복사 실패 - thisType 때문");
            // 소스코드 참고: https://medium.com/chenjd-xyz/unity-tip-use-trygetcomponent-instead-of-getcomponent-to-avoid-memory-allocation-in-the-editor-fe0c3121daf6
        }


    }
}