﻿using System.Collections.Generic;
using Replay.Common.Static;
using Replay.Data.Interface;
using UnityEngine;

namespace Replay.Data
{
    public class ArchiveAllInstancesSnapshot : IArchive
    {
        /// <summary> 시퀀스별 프리팹 인스턴스 스냅샷 딕셔너리 - Key: {Sequence}, Value: {DicAllInstancesTemporary} </summary>
        public Dictionary<int, Dictionary<string, string>> DicAllInstancesSnapshot { get; private set; }
        
        /// <summary>
        /// 초기화
        /// </summary>
        public ArchiveAllInstancesSnapshot()
        {
            this.DicAllInstancesSnapshot = new Dictionary<int, Dictionary<string, string>>();
        }
        
        /// <summary>
        /// 시퀀스별 프리팹 인스턴스 스냅샷 딕셔너리 초기화
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void TryAdd(int key, Dictionary<string, string> value)
        {
            if (this.DicAllInstancesSnapshot.ContainsKey(key))
            {
                Debug.LogError($"현재 프레임의 스냅샷이 이미 저장되어 있는데, 중복 저장을 시도했습니다. sequenceKey: {key}");
                return;
            }
            this.DicAllInstancesSnapshot.Add(key, value);
        }
        
        /// <summary>
        /// 시퀀스별 프리팹 인스턴스 스냅샷 딕셔너리 저장
        /// </summary>
        public void FileSave(string filePath)
        {
            ES3.Save(GlobalConstants.SAVE_KEY_ALL_INSTANCES_SNAPSHOT, this.DicAllInstancesSnapshot, filePath, GlobalConstants.GlobalES3Settings);
        }

        /// <summary>
        /// 시퀀스별 프리팹 인스턴스 스냅샷 딕셔너리 불러오기
        /// </summary>
        public void FileLoad(string filePath)
        {
            this.DicAllInstancesSnapshot = ES3.Load(GlobalConstants.SAVE_KEY_ALL_INSTANCES_SNAPSHOT, filePath, GlobalConstants.GlobalES3Settings) as Dictionary<int, Dictionary<string, string>>;
        }
    }
}