﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Replay.Common.Static;
using Replay.Data.Interface;
using UnityEngine;

namespace Replay.Data
{
    public class ArchiveCustomGameObjectDataSnapshots : IArchive
    {
        /// <summary> </summary>
        public Dictionary<string, Dictionary<string, Dictionary<int, GameObject>>> DicCustomGameObjectDataSnapshot { get; private set; }
        
        /// <summary>
        /// 초기화
        /// </summary>
        public ArchiveCustomGameObjectDataSnapshots()
        {
            this.DicCustomGameObjectDataSnapshot = new Dictionary<string, Dictionary<string, Dictionary<int, GameObject>>>();
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="prefabKey"></param>
        /// <param name="gameObjectName"></param>
        public void TryAdd(string prefabKey, string gameObjectName, Dictionary<int, GameObject> dic = null)
        {
            if (!this.DicCustomGameObjectDataSnapshot.ContainsKey(prefabKey))
            {
                // Key가 없으면 Key를 생성한다. 
                this.DicCustomGameObjectDataSnapshot.Add(prefabKey, new Dictionary<string, Dictionary<int, GameObject>>());
            }
            
            if (!this.DicCustomGameObjectDataSnapshot[prefabKey].ContainsKey(gameObjectName))
            {
                // Key가 없으면 Key를 생성한다. 
                this.DicCustomGameObjectDataSnapshot[prefabKey].Add(gameObjectName, new Dictionary<int, GameObject>());
            }
            
            if (dic != null)
            {
                this.DicCustomGameObjectDataSnapshot[prefabKey][gameObjectName] = dic;
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        /// <param name="rootPath"></param>
        public void FileSave(string rootPath)
        {
            var listPrefabKey = DicCustomGameObjectDataSnapshot.Select(kvp => kvp.Key).ToList();
            foreach (var prefabKey in listPrefabKey)
            {
                var prefabDirectoryPath = Path.Combine(rootPath, prefabKey);
                if (!Directory.Exists(prefabDirectoryPath))
                {
                    Directory.CreateDirectory(prefabDirectoryPath);
                }
                var listGameObjectName = DicCustomGameObjectDataSnapshot[prefabKey].Select(kvp => kvp.Key).ToList();
                
                foreach (var gameObjectName in listGameObjectName)
                {
                    var objFilePath = Path.Combine(prefabDirectoryPath, $"{gameObjectName}.ecs");
                    ES3.Save(GlobalConstants.SAVE_KEY_CUSTOM_GAMEOBJECT_DATA_SNAPSHOT, this.DicCustomGameObjectDataSnapshot[prefabKey][gameObjectName], objFilePath, GlobalConstants.GlobalES3Settings);
                    Debug.Log($"DataSave Complete. Key: {gameObjectName}, Count: {DicCustomGameObjectDataSnapshot[prefabKey][gameObjectName].Count}");
                    this.DicCustomGameObjectDataSnapshot[prefabKey][gameObjectName] = new Dictionary<int, GameObject>();
                    // 저장완료 알림
                }
            }
            
            var frameFilePath = Path.Combine(rootPath, GlobalConstants.FILE_NAME_PREFABKEY_GAMEOBJECTNAME_PAIR);
            ES3.Save(GlobalConstants.SAVE_KEY_CUSTOM_GAMEOBJECTS_DATA_SNAPSHOT_ALL, this.DicCustomGameObjectDataSnapshot, frameFilePath, GlobalConstants.GlobalES3Settings);
        }
        
        /// <summary>
        /// 불러오기
        /// </summary>
        public void FileLoad(string rootPath)
        {
            var frameFilePath = Path.Combine(rootPath, GlobalConstants.FILE_NAME_PREFABKEY_GAMEOBJECTNAME_PAIR);
            this.DicCustomGameObjectDataSnapshot = ES3.Load(GlobalConstants.SAVE_KEY_CUSTOM_GAMEOBJECTS_DATA_SNAPSHOT_ALL, frameFilePath, GlobalConstants.GlobalES3Settings) as Dictionary<string, Dictionary<string, Dictionary<int, GameObject>>>;
            if (this.DicCustomGameObjectDataSnapshot == null) return;
            var listPrefabKey = DicCustomGameObjectDataSnapshot.Select(kvp => kvp.Key).ToList();

            int total = 0;
            foreach (var prefabKey in listPrefabKey)
            {
                total += DicCustomGameObjectDataSnapshot[prefabKey].Count;
            }
            Debug.Log($"전체 Load할 GameObject 수: {total}");

            int count = 0;
            foreach (var prefabKey in listPrefabKey)
            {
                var prefabDirectoryPath = Path.Combine(rootPath, prefabKey);
                var listGameObjectName = DicCustomGameObjectDataSnapshot[prefabKey].Select(kvp => kvp.Key).ToList();
                
                foreach (var gameObjectName in listGameObjectName)
                {
                    var objFilePath = Path.Combine(prefabDirectoryPath, $"{gameObjectName}.ecs");
                    DicCustomGameObjectDataSnapshot[prefabKey][gameObjectName] =
                        (Dictionary<int, GameObject>) ES3.Load(GlobalConstants.SAVE_KEY_CUSTOM_GAMEOBJECT_DATA_SNAPSHOT, objFilePath, GlobalConstants.GlobalES3Settings);
                    
                    //Debug.Log($"DataLoad Complete. Key: {gameObjectName}, Count: {DicCustomGameObjectDataSnapshot[prefabKey][gameObjectName].Count}");
                    count++;
                    Debug.Log($"현재 Load한 GameObject 수: {count}, 진행률: {((float)count*100f)/((float)total)}");
                    // 불러오기 완료 알림
                }
            }
            // https://stackoverflow.com/questions/3968543/convert-dictionary-to-list-collection-in-c-sharp
        }
    }
}