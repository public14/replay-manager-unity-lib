﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Replay.Common.Static;
using Replay.Data.Interface;
using UnityEngine;

namespace Replay.Data
{
    public class ArchiveFullGameObjectDataSnapshots : IArchive
    {
        /// <summary> 게임오브젝트별로 시퀀스 스냅샷 데이터를 보관하는 딕셔너리 </summary>
        public Dictionary<string, Dictionary<string, Dictionary<int, GameObject>>> DicFullGameObjectDataSnapshots { get; private set; }
        
        /// <summary>
        /// 초기화
        /// </summary>
        public ArchiveFullGameObjectDataSnapshots()
        {
            this.DicFullGameObjectDataSnapshots = new Dictionary<string, Dictionary<string, Dictionary<int, GameObject>>>();
        }

        /// <summary>
        /// 딕셔너리에 데이터 추가하기
        /// </summary>
        /// <param name="prefabKey"></param>
        /// <param name="gameObjectName"></param>
        public void TryAdd(string prefabKey, string gameObjectName, Dictionary<int, GameObject> dic = null)
        {
            if (!this.DicFullGameObjectDataSnapshots.ContainsKey(prefabKey))
            {
                // Key가 없으면 Key를 생성한다. 
                this.DicFullGameObjectDataSnapshots.Add(prefabKey, new Dictionary<string, Dictionary<int, GameObject>>());
            }
            
            if (!this.DicFullGameObjectDataSnapshots[prefabKey].ContainsKey(gameObjectName))
            {
                // Key가 없으면 Key를 생성한다. 
                this.DicFullGameObjectDataSnapshots[prefabKey].Add(gameObjectName, new Dictionary<int, GameObject>());
            }

            if (dic != null)
            {
                this.DicFullGameObjectDataSnapshots[prefabKey][gameObjectName] = dic;
            }
        }
      
        /// <summary>
        /// 저장
        /// </summary>
        /// <param name="rootPath"></param>
        public void FileSave(string rootPath)
        {
            var listPrefabKey = this.DicFullGameObjectDataSnapshots.Select(kvp => kvp.Key).ToList();
            foreach (var prefabKey in listPrefabKey)
            {
                var prefabDirectoryPath = Path.Combine(rootPath, prefabKey);
                if (!Directory.Exists(prefabDirectoryPath))
                {
                    Directory.CreateDirectory(prefabDirectoryPath);
                }
                
                var listGameObjectName = this.DicFullGameObjectDataSnapshots[prefabKey].Select(kvp => kvp.Key).ToList();
                foreach (var gameObjectName in listGameObjectName)
                {
                    var objFilePath = Path.Combine(prefabDirectoryPath, $"{gameObjectName}.ecs");
                    ES3.Save(GlobalConstants.SAVE_KEY_FULL_GAMEOBJECT_DATA_SNAPSHOT, this.DicFullGameObjectDataSnapshots[prefabKey][gameObjectName], objFilePath, GlobalConstants.GlobalES3Settings);
                    Debug.Log($"DataSave Complete. Key: {gameObjectName}, Count: {this.DicFullGameObjectDataSnapshots[prefabKey][gameObjectName].Count}");
                    this.DicFullGameObjectDataSnapshots[prefabKey][gameObjectName] = new Dictionary<int, GameObject>(); // 값 초기화하기
                    // 저장완료 알림
                }
            }

            var frameFilePath = Path.Combine(rootPath, GlobalConstants.FILE_NAME_PREFABKEY_GAMEOBJECTNAME_PAIR);
            ES3.Save(GlobalConstants.SAVE_KEY_FULL_GAMEOBJECTS_DATA_SNAPSHOTS_ALL, this.DicFullGameObjectDataSnapshots, frameFilePath, GlobalConstants.GlobalES3Settings); // 값이 없는 빈 껍데기만 조회하기
        }
        
        /// <summary>
        /// 불러오기
        /// </summary>
        public void FileLoad(string rootPath)
        {
            var frameFilePath = Path.Combine(rootPath, GlobalConstants.FILE_NAME_PREFABKEY_GAMEOBJECTNAME_PAIR);
            this.DicFullGameObjectDataSnapshots = ES3.Load(GlobalConstants.SAVE_KEY_FULL_GAMEOBJECTS_DATA_SNAPSHOTS_ALL, frameFilePath, GlobalConstants.GlobalES3Settings) as Dictionary<string, Dictionary<string, Dictionary<int, GameObject>>>;
            if (this.DicFullGameObjectDataSnapshots == null) return;
            var listPrefabKey = this.DicFullGameObjectDataSnapshots.Select(kvp => kvp.Key).ToList();
            foreach (var prefabKey in listPrefabKey)
            {
                var prefabDirectoryPath = Path.Combine(rootPath, prefabKey);
                var listGameObjectName = this.DicFullGameObjectDataSnapshots[prefabKey].Select(kvp => kvp.Key).ToList();
                
                foreach (var gameObjectName in listGameObjectName)
                {
                    var objFilePath = Path.Combine(prefabDirectoryPath, $"{gameObjectName}.ecs");
                    this.DicFullGameObjectDataSnapshots[prefabKey][gameObjectName] =
                        (Dictionary<int, GameObject>) ES3.Load(GlobalConstants.SAVE_KEY_FULL_GAMEOBJECT_DATA_SNAPSHOT, objFilePath, GlobalConstants.GlobalES3Settings);
                    Debug.Log($"DataLoad Complete. Key: {gameObjectName}, Count: {this.DicFullGameObjectDataSnapshots[prefabKey][gameObjectName].Count}");
                    // 불러오기 완료 알림
                }
            }

            // https://stackoverflow.com/questions/3968543/convert-dictionary-to-list-collection-in-c-sharp
        }
    }
}