﻿using System.Collections.Generic;
using Replay.Common.Static;
using Replay.Data.Interface;

namespace Replay.Data
{
    public class ArchiveMaxCount : IArchive
    {
        /// <summary> 프리팹별로 생성된 최대 인스턴스 갯수 Key: {프리팹키}, Value: {인스턴스수} </summary>
        public Dictionary<string, int> DicMaxCount { get; private set; }

        /// <summary>
        /// 프리팹 맥스수량 초기화
        /// </summary>
        public ArchiveMaxCount()
        {
            this.DicMaxCount = new Dictionary<string, int>();
        }

        /// <summary>
        /// 프리팹 맥스수량 추가
        /// </summary>
        /// <param name="key"></param>
        /// <param name="newMaxCount"></param>
        public void TryAdd(string key, int newMaxCount)
        {
            if (!this.DicMaxCount.ContainsKey(key))
            {
                // Key가 없으면 Key를 일단 생성한다. 
                this.DicMaxCount.Add(key, 0);
            }
            
            //int newMaxCount = this.GetMaxInstanceCount(prefabKey);
            int lastMaxCount = this.DicMaxCount[key];
            if (newMaxCount > lastMaxCount)
            {
                this.DicMaxCount[key] = newMaxCount; // 현재 조회한 MaxCount가 더 크다면 Update한다. 
                // Debug.Log($"Instance Max Count Updated from {lastMaxCount} to {newMaxCount}");
            }
        }
        
        /// <summary>
        /// 프리팹 맥스수량 저장
        /// </summary>
        public void FileSave(string filePath)
        {
            ES3.Save(GlobalConstants.SAVE_KEY_INSTANCE_MAX_COUNT, this.DicMaxCount, filePath, GlobalConstants.GlobalES3Settings);
        }

        /// <summary>
        /// 프리팹 맥스수량 불러오기
        /// </summary>
        public void FileLoad(string filePath)
        {
            this.DicMaxCount = ES3.Load(GlobalConstants.SAVE_KEY_INSTANCE_MAX_COUNT, filePath, GlobalConstants.GlobalES3Settings) as Dictionary<string, int>;
        }
    }
}