﻿using System.Collections.Generic;
using Replay.Common.Static;
using Replay.Data.Interface;

namespace Replay.Data
{
    /// <summary>
    /// 프리팹 보관 클래스
    /// </summary>
    public class ArchivePrefab : IArchive
    {
        /// <summary> 프리팹키 리스트 </summary>
        public List<string> ListPrefabKey { get; private set; }

        /// <summary>
        /// 프리팹키 리스트 초기화
        /// </summary>
        public ArchivePrefab()
        {
            // this.DicPrefab = new Dictionary<string, GameObject>();
            ListPrefabKey = new List<string>();
        }
        
        /// <summary>
        /// 프리팹키 리스트 추가
        /// </summary>
        /// <param name="key"></param>
        public void TryAdd(string key)
        {
            if (!this.ListPrefabKey.Exists(x => x == key))
            {
                this.ListPrefabKey.Add(key);
            }
        }
        
        /// <summary>
        /// 프리팹키 리스트 저장
        /// </summary>
        /// <param name="filePath"></param>
        public void FileSave(string filePath)
        {
            ES3.Save(GlobalConstants.SAVE_KEY_PREFAB_KEY_LIST, this.ListPrefabKey, filePath, GlobalConstants.GlobalES3Settings);
        }

        /// <summary>
        /// 프리팹키 리스트 불러오기
        /// </summary>
        /// <param name="filePath"></param>
        public void FileLoad(string filePath)
        {
            this.ListPrefabKey = ES3.Load(GlobalConstants.SAVE_KEY_PREFAB_KEY_LIST, filePath, GlobalConstants.GlobalES3Settings) as List<string>;
        }
    }
}