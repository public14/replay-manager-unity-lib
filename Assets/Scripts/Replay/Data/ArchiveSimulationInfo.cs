﻿using System;
using Replay.Common.Enum;
using Replay.Common.Static;
using Replay.Data.Interface;

namespace Replay.Data
{
    public class ArchiveSimulationInfo : IArchive
    {
        public string PlayId { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public long StartUnixTime { get; set; }
        public long EndUnixTime { get; set; }
        public int EndSequence { get; set; }
        public float SavingTimeInterval { get; set; }
        public SaveDataUnit SaveDataUnit { get; set; }
        
        public void FileSave(string filePath)
        {
            ES3.Save(GlobalConstants.SAVE_KEY_PLAY_ID, this.PlayId, filePath, GlobalConstants.GlobalES3Settings);
            ES3.Save(GlobalConstants.SAVE_KEY_START_DATE_TIME, this.StartDateTime, filePath, GlobalConstants.GlobalES3Settings);
            ES3.Save(GlobalConstants.SAVE_KEY_END_DATE_TIME, this.EndDateTime, filePath, GlobalConstants.GlobalES3Settings);
            ES3.Save(GlobalConstants.SAVE_KEY_START_UNIX_TIME, this.StartUnixTime, filePath, GlobalConstants.GlobalES3Settings);
            ES3.Save(GlobalConstants.SAVE_KEY_END_UNIX_TIME, this.EndUnixTime, filePath, GlobalConstants.GlobalES3Settings);
            ES3.Save(GlobalConstants.SAVE_KEY_END_SEQUENCE, this.EndSequence, filePath, GlobalConstants.GlobalES3Settings);
            ES3.Save(GlobalConstants.SAVE_KEY_SAVING_TIME_INTERVAL, this.SavingTimeInterval, filePath, GlobalConstants.GlobalES3Settings);
            ES3.Save(GlobalConstants.SAVE_KEY_SAVE_DATA_UNIT, this.SaveDataUnit, filePath, GlobalConstants.GlobalES3Settings);
        }

        /// <summary>
        /// 프리팹 맥스수량 불러오기
        /// </summary>
        public void FileLoad(string filePath)
        {
            this.PlayId = (string)ES3.Load(GlobalConstants.SAVE_KEY_PLAY_ID, filePath, GlobalConstants.GlobalES3Settings);
            this.StartDateTime = (DateTime)ES3.Load(GlobalConstants.SAVE_KEY_START_DATE_TIME, filePath, GlobalConstants.GlobalES3Settings);
            this.EndDateTime = (DateTime)ES3.Load(GlobalConstants.SAVE_KEY_END_DATE_TIME, filePath, GlobalConstants.GlobalES3Settings);
            this.StartUnixTime = (long)ES3.Load(GlobalConstants.SAVE_KEY_START_UNIX_TIME, filePath, GlobalConstants.GlobalES3Settings);
            this.EndUnixTime = (long)ES3.Load(GlobalConstants.SAVE_KEY_END_UNIX_TIME, filePath, GlobalConstants.GlobalES3Settings);
            this.EndSequence = (int)ES3.Load(GlobalConstants.SAVE_KEY_END_SEQUENCE, filePath, GlobalConstants.GlobalES3Settings);
            this.SavingTimeInterval = (float)ES3.Load(GlobalConstants.SAVE_KEY_SAVING_TIME_INTERVAL, filePath, GlobalConstants.GlobalES3Settings);
            this.SaveDataUnit = (SaveDataUnit)ES3.Load(GlobalConstants.SAVE_KEY_SAVE_DATA_UNIT, filePath, GlobalConstants.GlobalES3Settings);
        }
    }
}