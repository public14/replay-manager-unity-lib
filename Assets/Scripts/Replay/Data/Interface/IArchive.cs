﻿namespace Replay.Data.Interface
{
    public interface IArchive
    {
        void FileSave(string filePath);
        void FileLoad(string filePath);
    }
}