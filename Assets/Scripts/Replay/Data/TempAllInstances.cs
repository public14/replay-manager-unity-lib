﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Replay.Data
{
    public class TempAllInstances
    {
        /// <summary> 프리팹별 현재 씬에 활성화된 전체 인스턴스 리스트 딕셔너리 - Key: {프리팹키}, Value: {프리팹 인스턴스명 리스트} </summary>
        public Dictionary<string, string> DicAllInstancesTemporary { get; private set; }

        /// <summary>
        /// 초기화
        /// </summary>
        public TempAllInstances()
        {
            this.DicAllInstancesTemporary = new Dictionary<string, string>();
        }

        /// <summary>
        /// 추가
        /// </summary>
        /// <param name="prefabKey"></param>
        /// <param name="gameObjectName"></param>
        public void TryAdd(string prefabKey, string gameObjectName)
        {
            if (!this.DicAllInstancesTemporary.ContainsKey(prefabKey))
            {
                // Key가 없으면 Key를 일단 생성한다. 
                this.DicAllInstancesTemporary.Add(prefabKey, string.Empty);
            }

            if (this.DicAllInstancesTemporary[prefabKey].Contains(gameObjectName))
            {
                // 동일한 게임오브젝트명을 이미 가지고 있다면 에러를 뱉는다.
                Debug.LogError($"GameObject instance name is duplicated in sInstanceNames. this.DicInstanceNamesByPrefab[data.PrefabKey] : {this.DicAllInstancesTemporary[prefabKey]}, data.PrefabKey: {prefabKey}, data.GameObjectName: {gameObjectName}");
                return;
            }

            // DicAllInstancesTemporary[prefabKey]에는 다음과 같은 형태로 데이터가 보관된다.
            // Ex. TRY-MID-199;TRY-MID-191;TRY-MID-099;
            // Ex. MSBX1;MSBX2;MSBX13;MSBX153
            
            this.DicAllInstancesTemporary[prefabKey] += gameObjectName + ";";
            Debug.Log($"_dicInstanceNamesByPrefab Add Executed: {gameObjectName} \r\n TempData: PrefabKey: {prefabKey}, ObjectNameList: {this.DicAllInstancesTemporary[prefabKey]}");
        }
        
        /// <summary>
        /// 삭제
        /// </summary>
        /// <param name="prefabKey"></param>
        /// <param name="gameObjectName"></param>
        public void TryDelete(string prefabKey, string gameObjectName)
        {
            if (!this.DicAllInstancesTemporary.ContainsKey(prefabKey))
            {
                // Key가 없으면 에러를 뱉는다.
                Debug.LogError(
                    $"삭제하려는 게임오브젝트가 _dicInstanceListByPrefab[data.PrefabName]에서 PrefabKey로 조회가 되지 않습니다. PrefabKey : {prefabKey}");
                return;
            }

            if (!this.DicAllInstancesTemporary[prefabKey].Contains(gameObjectName))
            {
                // 게임오브젝트명이 검색되지 않으면 에러를 뱉는다.
                Debug.LogError(
                    $"_dicInstanceListByPrefab[data.PrefabName]에 게임오브젝트명이 등록되어 있지 않으나, 삭제를 시도했습니다. PrefabKey: {prefabKey}, GameObjectName: {gameObjectName}");
                return;
            }
            this.DicAllInstancesTemporary[prefabKey] = this.DicAllInstancesTemporary[prefabKey].Replace(gameObjectName + ";", "");
            Debug.Log($"_dicInstanceNamesByPrefab Delete Executed: {gameObjectName} \r\n TempData: PrefabKey: {prefabKey}, ObjectNameList: {this.DicAllInstancesTemporary[prefabKey]}");
        }

        public int GetMaxInstanceCount(string prefabKey)
        {
            MatchCollection matches = Regex.Matches(this.DicAllInstancesTemporary[prefabKey], ";");
            return matches.Count;
        }
    }
}