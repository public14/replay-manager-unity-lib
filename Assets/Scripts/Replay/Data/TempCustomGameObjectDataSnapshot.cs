﻿using System.Collections.Generic;
using UnityEngine;

namespace Replay.Data
{
    public class TempCustomGameObjectDataSnapshot
    {
        public Dictionary<int, GameObject> DicCustomGameObjectDataSnapshot { get; set; }
        
        public TempCustomGameObjectDataSnapshot()
        {
            this.DicCustomGameObjectDataSnapshot = new Dictionary<int, GameObject>();
        }

        public void TryAdd(int key, GameObject value)
        {
            if (!this.DicCustomGameObjectDataSnapshot.ContainsKey(key))
            {
                this.DicCustomGameObjectDataSnapshot.Add(key, value);
            }
        }
    }
}