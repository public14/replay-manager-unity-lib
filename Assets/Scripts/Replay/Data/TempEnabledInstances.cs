﻿using System.Collections.Generic;
using UnityEngine;

namespace Replay.Data
{
    public class TempEnabledInstances
    {
        /// <summary> 프리팹별 현재 씬에 활성화된 전체 인스턴스 리스트 딕셔너리 - Key: {프리팹키}, Value: {프리팹 인스턴스명 리스트} </summary>
        public Dictionary<string, string> DicEnabledInstancesTemporary { get; private set; }

        /// <summary> 초기화 </summary>
        public TempEnabledInstances()
        {
            this.DicEnabledInstancesTemporary = new Dictionary<string, string>();
        }

        /// <summary>
        /// 추가
        /// </summary>
        /// <param name="prefabKey"></param>
        /// <param name="gameObjectName"></param>
        public void TryAdd(string prefabKey, string gameObjectName)
        {
            if (!this.DicEnabledInstancesTemporary.ContainsKey(prefabKey))
            {
                // Key가 없으면 Key를 일단 생성한다. 
                this.DicEnabledInstancesTemporary.Add(prefabKey, string.Empty);
            }

            if (this.DicEnabledInstancesTemporary[prefabKey].Contains(gameObjectName))
            {
                // 동일한 게임오브젝트명을 이미 가지고 있다면 에러를 뱉는다.
                Debug.LogError("GameObject instance name is duplicated in sInstanceNames");
                return;
            }

            this.DicEnabledInstancesTemporary[prefabKey] += gameObjectName + ";";
        }
    }
}