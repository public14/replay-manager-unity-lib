﻿using System.Collections.Generic;
using UnityEngine;

namespace Replay.Data
{
    public class TempFullGameObjectDataSnapshot
    {
        public Dictionary<int, GameObject> DicFullGameObjectDataSnapshot { get; set; }
        
        public TempFullGameObjectDataSnapshot()
        {
            this.DicFullGameObjectDataSnapshot = new Dictionary<int, GameObject>();
        }

        public void TryAdd(int key, GameObject value)
        {
            if (!this.DicFullGameObjectDataSnapshot.ContainsKey(key))
            {
                this.DicFullGameObjectDataSnapshot.Add(key, value);
            }
        }
    }
}