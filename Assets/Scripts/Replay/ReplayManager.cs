﻿using System;
using System.Collections.Generic;
using com.ootii.Messages;
using MEC;
using Replay.Common.Enum;
using Replay.Common.Messages;
using Replay.Common.Static;
using Replay.Core;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;
using UnityEngine.Serialization;

namespace Replay
{
    public class ReplayManager : MonoBehaviour
    {
        [System.Serializable] 
        public class CustomSerializableDictionary : SerializableDictionaryBase<string, GameObject> { } // Inspector 창에 직렬화 가능한 Dictionary 생성

        [FormerlySerializedAs("replayCentralCore")] public ReplayCentralCommon replayCentralCommon; // Replay 코어 등록 

        [Header("리플레이 모드를 설정하세요.")] [SerializeField]
        public ReplayMode replayMode = ReplayMode.Save;

        [Range(0.01f, 1f)]
        [Header("저장할 시간의 단위를 입력하세요.(원활한 재생 위해 0.1 이상 권장)")] [SerializeField]
        public float savingTimeInterval = 0.1f;

        [Header("리플레이 재생일 경우, 재생할 리플레이의 Key를 입력하세요.")] [SerializeField]
        public string replayKey = string.Empty; // 디렉토리를 읽어서 마지막 기록을 가져오도록 해보기

        [Header("리플레이 재생일 경우, 재생할 리플레이의 배속을 입력하세요.")] [SerializeField]
        public ReplaySpeed replaySpeed = ReplaySpeed.Origin; 

        [Header("프리팹을 딕셔너리에 등록하세요.")] [SerializeField]
        public CustomSerializableDictionary dicPrefab = new CustomSerializableDictionary();
        
        static ReplayManager _instance = null; // 싱글톤 패턴을 위한 static 변수 선언

        public int localSaverEndCount = 0;

        /// <summary> 싱글톤 객체 </summary>
        public static ReplayManager Instance()
        {
            return _instance;
        }
        
        void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
                DontDestroyOnLoad(this.gameObject); // 씬이 바뀌어도 죽지 않도록 설정
            }
            else
            {
                if (this != _instance)
                {
                    Destroy(this.gameObject);
                }
            }

            this.InitializeEventListeners(); // 이벤트 리스너 등록
        }

        /// <summary>
        /// 이벤트 리스너 초기화
        /// </summary>
        private void InitializeEventListeners()
        {
            MessageDispatcher.AddListener(GlobalConstants.EVENT_KEY_SIMULATION_START, OnSimulationStart); // 시뮬레이션 시작 알림 이벤트 등록
            MessageDispatcher.AddListener(GlobalConstants.EVENT_KEY_SIMULATION_END, OnSimulationEnd); // 시뮬레이션 종료 알림 이벤트 등록
            MessageDispatcher.AddListener(GlobalConstants.EVENT_KEY_REPLAY_DATA_LOAD, OnReplayDataLoad); // 리플레이 데이터 로드 이벤트 등록
            MessageDispatcher.AddListener(GlobalConstants.EVENT_KEY_REPLAY_START, OnReplayStart); // 리플레이 시작 알림 이벤트 등록
            MessageDispatcher.AddListener(GlobalConstants.EVENT_KEY_REPLAY_END, OnReplayEnd); // 리플레이 종료 알림 이벤트 등록
        }

        /// <summary>
        /// 시뮬레이션 시작
        /// </summary>
        /// <param name="incomingMessage"></param>
        private void OnSimulationStart(IMessage incomingMessage)
        {
            Debug.Log("Simulation Start");
            var eventData = (SimulationStartDataObject) incomingMessage.Data;
            this.replayMode = ReplayMode.Save;
            
            this.replayCentralCommon = ReplayCentralFactory.Create(this.replayMode, eventData.PlayId, this.savingTimeInterval, eventData.StartUnixTime);
            MessageDispatcher.AddListener(GlobalConstants.EVENT_KEY_ENABLE_GAMEOBJECT, this.replayCentralCommon.OnEventEnableGameObject); // 프리팹 활성화(생성 포함) 이벤트 등록 
            MessageDispatcher.AddListener(GlobalConstants.EVENT_KEY_DISABLE_GAMEOBJECT, this.replayCentralCommon.OnEventDisableGameObject); // 프리팹 비활성화(파괴 포함) 이벤트 등록
            // this.replayCentralCommon.OnStart();
            
            var replayLocalManagers = GameObject.FindObjectsOfType<ReplayPrefabKeyRegister>();
            foreach (var replayLocalManager in replayLocalManagers)
            {
                replayLocalManager.gameObject.AddComponent<ReplayDataCollector>();
            }
        }

        /// <summary>
        /// 시뮬레이션 종료
        /// </summary>
        /// <param name="incomingMessage"></param>
        private void OnSimulationEnd(IMessage incomingMessage)
        {
            Debug.Log("Simulation End");

            // 먼저, LocalSaver들을 수집하여 OnEnd한다. 
            var replayLocalSavers = GameObject.FindObjectsOfType<ReplayDataCollector>();
            foreach (var replayLocalSaver in replayLocalSavers)
            {
                replayLocalSaver.OnEndCollecting();
            }
            
            // 다음으로는 CentralCore(CentralSaver)를 End한다. 
            Timing.RunCoroutine(OnEndCentralCore(replayLocalSavers.Length), GlobalConstants.COROUTINE_TAG_CENTRAL_SIMULATION_END);

            // 모든 Save 시작 
            // Save 진행률(%) 파악 
            // 만약에 종료 신호가 없다면, OnDestroy 시점에 종료하기 
        }

        
        private IEnumerator<float> OnEndCentralCore(int totalCount)
        {
            // localSaver가 종료된 숫자를 카운트하여, 토탈카운트보다 작으면 계속 대기시킨다. 
            while (this.localSaverEndCount < totalCount)
            {
                yield return Timing.WaitForSeconds(0.5f);
            }

            this.replayCentralCommon.OnEnd(); // 모든 데이터를 저장한다. 
            Timing.KillCoroutines(GlobalConstants.COROUTINE_TAG_CENTRAL_SIMULATION_END); // 코루틴 종료
            Debug.Log($"OnEndCentralCore Killed. localSaverEndCount:{this.localSaverEndCount}, totalCount: {totalCount}");
        }

        /// <summary>
        /// 리플레이 데이터 로드
        /// </summary>
        /// <param name="incomingMessage"></param>
        private void OnReplayDataLoad(IMessage incomingMessage)
        {
            Debug.Log("Replay Data Load");
            var eventData = (string) incomingMessage.Data;
            this.replayMode = ReplayMode.Load;
            this.replayCentralCommon = ReplayCentralFactory.Create(this.replayMode, eventData);
        }

        /// <summary>
        /// 리플레이 시작
        /// </summary>
        /// <param name="incomingMessage"></param>
        private void OnReplayStart(IMessage incomingMessage)
        {
            Debug.Log("Replay Start");
            this.replayCentralCommon.OnStart();
        }
        
        /// <summary>
        /// 리플레이 종료
        /// </summary>
        /// <param name="incomingMessage"></param>
        private void OnReplayEnd(IMessage incomingMessage)
        {
            Debug.Log("Replay End");
            this.replayCentralCommon.OnEnd();
        }
        
        
        void Update()
        {
            // 숫자 1번 - 시뮬레이션 시작
            if(Input.GetKeyUp(KeyCode.Alpha1))
            {
                var startDataObject = new SimulationStartDataObject(Guid.NewGuid().ToString(), DateTimeOffset.Now.ToUnixTimeMilliseconds());
                MessageDispatcher.SendMessageData(GlobalConstants.EVENT_KEY_SIMULATION_START, startDataObject);
            }
            
            // 숫자 2번 - 시뮬레이션 종료 및 저장
            if(Input.GetKeyUp(KeyCode.Alpha2))
            {
                MessageDispatcher.SendMessage(GlobalConstants.EVENT_KEY_SIMULATION_END);
            }
            
            // 숫자 3번 - 리플레이 데이터 로드
            if(Input.GetKeyUp(KeyCode.Alpha3))
            {
                var playId = this.replayKey;
                MessageDispatcher.SendMessageData(GlobalConstants.EVENT_KEY_REPLAY_DATA_LOAD, playId);
            }
            
            // 숫자 4번 - 리플레이 재생 시작
            if(Input.GetKeyUp(KeyCode.Alpha4))
            {
                MessageDispatcher.SendMessage(GlobalConstants.EVENT_KEY_REPLAY_START);
            }
            
            // 숫자 4번 - 리플레이 재생 종료
            if(Input.GetKeyUp(KeyCode.Alpha5))
            {
                MessageDispatcher.SendMessage(GlobalConstants.EVENT_KEY_REPLAY_END);
            }
        }

    }
}