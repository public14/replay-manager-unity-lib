﻿using Replay.Common.Static;
using Replay.Core;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace Replay
{
    public class ReplayPrefabKeyRegister : MonoBehaviour
    {
        public string prefabKey;
        
        void Awake()
        {
            if (!string.IsNullOrEmpty(prefabKey)) return;
            Debug.LogError(GlobalLogMessages.MESSAGE_PREFAB_KEY_IS_NULL);
        }
    }
}